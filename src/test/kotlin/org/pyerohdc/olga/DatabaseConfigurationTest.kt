package org.pyerohdc.olga

import com.esotericsoftware.minlog.Log
import org.flywaydb.core.api.logging.LogFactory
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.pyerohdc.olga.log.FlywayLogger

internal class DatabaseConfigurationTest {

    @BeforeEach
    fun setUp() {
        Log.INFO()
        LogFactory.setFallbackLogCreator(FlywayLogger())
    }

    @Test
    fun shouldStart() {
        val config = DatabaseConfiguration(Olga.getJarFolderPath().resolve("olga").toString())

        config.close()
    }

}
