package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity

interface HasIdAndEntity<T : Entity<T>> : HasId, Entity<T>
