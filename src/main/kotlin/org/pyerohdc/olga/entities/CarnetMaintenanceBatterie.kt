package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.datetime
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar

interface CarnetMaintenanceBatterieBase : HasId, CarnetBatterie

interface CarnetMaintenanceBatterie : HasIdAndEntity<CarnetMaintenanceBatterie>, CarnetMaintenanceBatterieBase {
    companion object : Entity.Factory<CarnetMaintenanceBatterie>()
}

object CarnetMaintenanceBatteries : Table<CarnetMaintenanceBatterie>("CARNET_MAINTENANCE_BATTERIE") {
    val id by int("ID").primaryKey().bindTo { it.id }
    val batterieId by int("BATTERIE_ID").references(Batteries) { it.batterie }
    val telepiloteId by int("TELEPILOTE_ID").references(Telepilotes) { it.telepilote }
    val date by datetime("DATE").bindTo { it.date }
    val notes by varchar("NOTES").bindTo { it.notes }
}
