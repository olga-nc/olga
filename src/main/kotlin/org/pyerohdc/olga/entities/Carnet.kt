package org.pyerohdc.olga.entities

import java.time.LocalDateTime

interface Carnet {
    var telepilote: Telepilote
    var date: LocalDateTime
    var notes: String?
}
