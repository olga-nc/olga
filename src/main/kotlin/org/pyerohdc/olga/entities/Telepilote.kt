package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.boolean
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar

interface TelepiloteBase : HasId, HasActif {
    var nom: String
    var prenom: String

    fun getNomPrenom() = "$nom $prenom"
    fun getPrenomNom() = "$prenom $nom"
}

interface Telepilote : HasIdAndEntity<Telepilote>, TelepiloteBase {
    companion object : Entity.Factory<Telepilote>()
}

object Telepilotes : Table<Telepilote>("TELEPILOTE") {
    val id by int("ID").primaryKey().bindTo { it.id }
    val nom by varchar("NOM").bindTo { it.nom }
    val prenom by varchar("PRENOM").bindTo { it.prenom }
    val actif by boolean("ACTIF").bindTo { it.actif }
}
