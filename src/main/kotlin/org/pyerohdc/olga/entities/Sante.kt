package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.datetime
import me.liuwj.ktorm.schema.int
import java.time.LocalDateTime

interface SanteBase : HasId {
    var batterie: Batterie
    var date: LocalDateTime
    var pourcentage: Int

    fun getSanteValueFromPourcentage() = SanteValue.fromPourcentage(pourcentage)
}

interface Sante : HasIdAndEntity<Sante>, SanteBase {
    companion object : Entity.Factory<Sante>()
}

object Santes : Table<Sante>("SANTE") {
    val id by int("ID").primaryKey().bindTo { it.id }
    val batterieId by int("BATTERIE_ID").references(Batteries) { it.batterie }
    val date by datetime("DATE").bindTo { it.date }
    val pourcentage by int("POURCENTAGE").bindTo { it.pourcentage }
}
