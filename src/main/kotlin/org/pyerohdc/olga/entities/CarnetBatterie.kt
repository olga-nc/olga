package org.pyerohdc.olga.entities

interface CarnetBatterie : Carnet {
    var batterie: Batterie
}
