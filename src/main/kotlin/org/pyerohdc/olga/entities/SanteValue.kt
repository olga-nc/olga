package org.pyerohdc.olga.entities

import javafx.scene.paint.Color

enum class SanteValue(val couleur: Color, val pourcentage: Int) {
    VERT(Color.FORESTGREEN, 75),
    JAUNE(Color.GOLD, 50),
    ORANGE(Color.DARKORANGE, 25),
    ROUGE(Color.RED, 0);

    companion object {
        fun fromPourcentage(pourcentage: Int) = values().first { pourcentage >= it.pourcentage }
    }
}
