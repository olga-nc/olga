package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.blob
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar

interface DocumentBase : HasId {
    var nom: String
    var format: String
    var doc: ByteArray
}

interface Document : HasIdAndEntity<Document>, DocumentBase {
    companion object : Entity.Factory<Document>()
}

object Documents : Table<Document>("DOCUMENT") {
    val id by int("ID").primaryKey().bindTo { it.id }
    val nom by varchar("NOM").bindTo { it.nom }
    val format by varchar("FORMAT").bindTo { it.format }
    val doc by blob("DOC").bindTo { it.doc }
}
