package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.boolean
import me.liuwj.ktorm.schema.datetime
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar
import java.time.LocalDateTime

interface BatterieBase : HasId, HasActif {
    var drone: Drone
    var numero: Int
    var numeroSerie: String
    var dateDernierCycle: LocalDateTime?
}

interface Batterie : HasIdAndEntity<Batterie>, BatterieBase {
    companion object : Entity.Factory<Batterie>()
}

object Batteries : Table<Batterie>("BATTERIE") {
    val id by int("ID").primaryKey().bindTo { it.id }
    val droneId by int("DRONE_ID").references(Drones) { it.drone }
    val numero by int("NUMERO").bindTo { it.numero }
    val numeroSerie by varchar("NUMERO_SERIE").bindTo { it.numeroSerie }
    val dateDernierCycle by datetime("DATE_DERNIER_CYCLE").bindTo { it.dateDernierCycle }
    val actif by boolean("ACTIF").bindTo { it.actif }
}
