package org.pyerohdc.olga.entities

interface HasActif {
    var actif: Boolean
}
