package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.datetime
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar
import java.time.LocalDateTime

interface VolBase : HasId {
    var telepilote: Telepilote
    var batterie: Batterie
    /**
     * En minutes
     */
    var duree: Int
    var date: LocalDateTime
    var chantier: String
    var client: String
    var incident: String?
    var commentaire: String?
}

interface Vol : HasIdAndEntity<Vol>, VolBase {
    companion object : Entity.Factory<Vol>()
}

object Vols : Table<Vol>("VOL") {
    val id by int("ID").primaryKey().bindTo { it.id }
    val telepiloteId by int("TELEPILOTE_ID").references(Telepilotes) { it.telepilote }
    val batterieId by int("BATTERIE_ID").references(Batteries) { it.batterie }
    val duree by int("DUREE").bindTo { it.duree }
    val date by datetime("DATE").bindTo { it.date }
    val chantier by varchar("CHANTIER").bindTo { it.chantier }
    val client by varchar("CLIENT").bindTo { it.client }
    val incident by varchar("INCIDENT").bindTo { it.incident }
    val commentaire by varchar("COMMENTAIRE").bindTo { it.commentaire }
}

data class VolData(
    override val id: Int,
    override var telepilote: Telepilote,
    override var batterie: Batterie,
    override var duree: Int,
    override var date: LocalDateTime,
    override var chantier: String,
    override var client: String,
    override var incident: String?,
    override var commentaire: String?
) : VolBase {
    constructor(base: VolBase) : this(
        base.id,
        base.telepilote,
        base.batterie,
        base.duree,
        base.date,
        base.chantier,
        base.client,
        base.incident,
        base.commentaire
    )
}
