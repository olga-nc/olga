package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.boolean
import me.liuwj.ktorm.schema.datetime
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar
import java.time.LocalDateTime

interface DroneBase : HasId, HasActif {
    var nom: String
    var matricule: String
    var dateDerniereMajControleur: LocalDateTime?
    var nombreCharges: Int

    fun getNomMatricule() = "$nom $matricule"
}

interface Drone : HasIdAndEntity<Drone>, DroneBase {
    companion object : Entity.Factory<Drone>()
}

object Drones : Table<Drone>("DRONE") {
    val id by int("ID").primaryKey().bindTo { it.id }
    val nom by varchar("NOM").bindTo { it.nom }
    val matricule by varchar("MATRICULE").bindTo { it.matricule }
    val dateDerniereMajControleur by datetime("DATE_DERNIERE_MAJ_CONTROLEUR").bindTo { it.dateDerniereMajControleur }
    val nombreCharges by int("NOMBRE_CHARGES").bindTo { it.nombreCharges }
    val actif by boolean("ACTIF").bindTo { it.actif }
}
