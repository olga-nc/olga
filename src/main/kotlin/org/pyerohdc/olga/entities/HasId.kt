package org.pyerohdc.olga.entities

interface HasId {
    val id: Int
}
