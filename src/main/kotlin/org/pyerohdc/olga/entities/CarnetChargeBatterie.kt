package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.datetime
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar

interface CarnetChargeBatterieBase : HasId, CarnetBatterie

interface CarnetChargeBatterie : HasIdAndEntity<CarnetChargeBatterie>, CarnetChargeBatterieBase {
    companion object : Entity.Factory<CarnetChargeBatterie>()
}

object CarnetChargeBatteries : Table<CarnetChargeBatterie>("CARNET_CHARGE_BATTERIE") {
    val id by int("ID").primaryKey().bindTo { it.id }
    val batterieId by int("BATTERIE_ID").references(Batteries) { it.batterie }
    val telepiloteId by int("TELEPILOTE_ID").references(Telepilotes) { it.telepilote }
    val date by datetime("DATE").bindTo { it.date }
    val notes by varchar("NOTES").bindTo { it.notes }
}
