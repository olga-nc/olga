package org.pyerohdc.olga.entities

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.datetime
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar

interface CarnetMaintenancePieceBase : HasId, Carnet {
    var drone: Drone
}

interface CarnetMaintenancePiece : HasIdAndEntity<CarnetMaintenancePiece>, CarnetMaintenancePieceBase {
    companion object : Entity.Factory<CarnetMaintenancePiece>()
}

object CarnetMaintenancePieces : Table<CarnetMaintenancePiece>("CARNET_MAINTENANCE_PIECE") {
    val id by int("ID").primaryKey().bindTo { it.id }
    val droneId by int("DRONE_ID").references(Drones) { it.drone }
    val telepiloteId by int("TELEPILOTE_ID").references(Telepilotes) { it.telepilote }
    val date by datetime("DATE").bindTo { it.date }
    val notes by varchar("NOTES").bindTo { it.notes }
}
