package org.pyerohdc.olga

object AppConstants {

    const val REPO_ID = "13791030"

    const val MILESTONES_BASE = "https://gitlab.com/olga-nc/olga/-/milestones/"

}
