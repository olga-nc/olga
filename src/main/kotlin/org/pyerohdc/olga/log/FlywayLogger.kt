package org.pyerohdc.olga.log

import org.flywaydb.core.api.logging.Log
import org.flywaydb.core.api.logging.LogCreator
import com.esotericsoftware.minlog.Log as MinLog

class FlywayLogger : LogCreator {
    override fun createLogger(clazz: Class<*>?): Log {
        return object : Log {
            override fun isDebugEnabled() = MinLog.DEBUG
            override fun debug(message: String?) = MinLog.debug("Flyway: ${clazz?.simpleName}", message)
            override fun info(message: String?) = MinLog.info("Flyway: ${clazz?.simpleName}", message)
            override fun warn(message: String?) = MinLog.warn("Flyway: ${clazz?.simpleName}", message)
            override fun error(message: String?) = MinLog.error("Flyway: ${clazz?.simpleName}", message)
            override fun error(message: String?, e: Exception?) =
                MinLog.error("Flyway: ${clazz?.simpleName}", message, e)
        }
    }
}
