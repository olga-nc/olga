package org.pyerohdc.olga.log

import com.esotericsoftware.minlog.Log
import org.pyerohdc.olga.Olga
import java.io.OutputStreamWriter
import java.nio.file.Files

class ConsoleLogger : Log.Logger()

class FileLogger : Log.Logger() {

    private val maxFileKeep = 5

    private val file = Olga.getJarFolderPath().resolve("olga.log").toFile()

    private val output: OutputStreamWriter

    init {
        if (file.exists()) {
            // Roll
            val logFiles =
                file.parentFile.listFiles { _, name -> name.matches(Regex("olga\\.log\\.[1-$maxFileKeep]")) }!!
            if (logFiles.isNotEmpty()) {
                logFiles
                    .find { it.name.endsWith(".$maxFileKeep") }
                    .let { it?.delete() }
                logFiles.filter { !it.name.endsWith(".$maxFileKeep") }
                    .sortedByDescending { it.name.split(".").last().toInt() }
                    .forEach {
                        it.toPath().let { p ->
                            Files.move(
                                p,
                                p.parent.resolve(
                                    p.fileName.toString().split(".").dropLast(1).joinToString(".")
                                            + "."
                                            + (p.fileName.toString().split(".").last().toInt() + 1)
                                )
                            )
                        }
                    }
            }
            file.toPath().let { p ->
                Files.move(p, p.parent.resolve(p.fileName.toString() + ".1"))
            }

            file.createNewFile()
        }
        output = file.writer()
    }

    override fun print(message: String) {
        output.write(message)
        output.write(System.lineSeparator())
        output.flush()
    }

}
