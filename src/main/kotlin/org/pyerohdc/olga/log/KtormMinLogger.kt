package org.pyerohdc.olga.log

import com.esotericsoftware.minlog.Log
import me.liuwj.ktorm.logging.Logger

class KtormMinLogger : Logger {

    companion object {
        const val TAG = "Ktorm"
    }

    override fun isTraceEnabled() = Log.TRACE

    override fun isDebugEnabled() = Log.DEBUG

    override fun isInfoEnabled() = Log.INFO

    override fun isWarnEnabled() = Log.WARN

    override fun isErrorEnabled() = Log.ERROR

    override fun trace(msg: String, e: Throwable?) = Log.trace(TAG, msg, e)

    override fun debug(msg: String, e: Throwable?) = Log.debug(TAG, msg, e)

    override fun info(msg: String, e: Throwable?) = Log.info(TAG, msg, e)

    override fun warn(msg: String, e: Throwable?) = Log.warn(TAG, msg, e)

    override fun error(msg: String, e: Throwable?) = Log.error(TAG, msg, e)
}
