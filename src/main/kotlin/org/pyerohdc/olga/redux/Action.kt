package org.pyerohdc.olga.redux

import org.pyerohdc.olga.entities.HasIdAndEntity

sealed class Action {
    override fun toString(): String = this.javaClass.simpleName
}

sealed class DbAction<T : HasIdAndEntity<T>>(val type: T) : Action()

data class Add<T : HasIdAndEntity<T>>(val value: T) : DbAction<T>(value)
data class Delete<T : HasIdAndEntity<T>>(val value: T) : DbAction<T>(value)
data class Modify<T : HasIdAndEntity<T>>(val oldValue: T, val newValue: T) : DbAction<T>(newValue)

object LoadAllData : Action()

object ClearAllData : Action()

object ReloadData : Action()
