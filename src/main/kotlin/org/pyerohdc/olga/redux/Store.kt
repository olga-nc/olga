package org.pyerohdc.olga.redux

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

interface Middleware<S, A> {
    fun dispatch(store: Store<S, A>, next: (A) -> Unit, action: A)

    fun combine(other: Middleware<S, A>): Middleware<S, A> = object : Middleware<S, A> {
        override fun dispatch(store: Store<S, A>, next: (A) -> Unit, action: A) =
            this@Middleware.dispatch(store, other.decompose()(store)(next), action)
    }

    fun decompose() =
        { s: Store<S, A> ->
            { next: (A) -> Unit ->
                { a: A ->
                    dispatch(s, next, a)
                }
            }
        }
}

class Store<S, A>(initialState: S, private val reducer: (S, A) -> S, private vararg val middlewares: Middleware<S, A>) {

    var state = initialState
        private set

    private val disposable: Disposable

    private val actions = PublishSubject.create<A>()

    val states: Observable<S> = actions.map {
        state = reducer(state, it)
        state
    }.publish().apply { disposable = connect() }


    fun dispatch(action: A) {
        middlewares.let {
            if (it.isEmpty()) null
            else it.reduce { acc, middleware -> acc.combine(middleware) }
        }
            ?.dispatch(this, actions::onNext, action)
            ?: actions.onNext(action)
    }
}
