package org.pyerohdc.olga.redux.logger

import org.pyerohdc.olga.redux.Middleware
import org.pyerohdc.olga.redux.Store
import org.pyerohdc.olga.redux.logger.Logger.Entry

fun <S : Any, A : Any> createLoggerMiddleware(
    logger: Logger<S>,
    diff: Boolean = false,
    predicate: (A, Store<S, A>) -> Boolean = { _, _ -> true },
    diffPredicate: (A, Store<S, A>) -> Boolean = { _, _ -> true }
): Middleware<S, A> {

    return object : Middleware<S, A> {
        override fun dispatch(store: Store<S, A>, next: (A) -> Unit, action: A) {
            next(action)
            if (predicate(action, store)) {
                val startTime = System.currentTimeMillis()
                val oldState = store.state
                val newState = store.state
                val endTime = System.currentTimeMillis()

                logger.log(
                    Entry(
                        action,
                        oldState,
                        newState,
                        startTime,
                        endTime,
                        endTime - startTime,
                        when {
                            diff && diffPredicate(action, store) -> Diff.calculate(
                                oldState,
                                newState
                            )
                            else -> null
                        }
                    )
                )
            }
        }
    }
}
