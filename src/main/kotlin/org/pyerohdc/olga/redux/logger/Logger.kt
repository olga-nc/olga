package org.pyerohdc.olga.redux.logger

import org.pyerohdc.olga.redux.logger.Diff.Change

interface Logger<in S : Any> {

    data class Entry<out S : Any>(
        val action: Any,
        val oldState: S,
        val newState: S,
        val startTime: Long,
        val endTime: Long,
        val duration: Long,
        val diff: List<Change>?
    )

    fun log(entry: Entry<S>)

    companion object {

        operator fun <S : Any> invoke(f: (entry: Entry<S>) -> Any?): Logger<S> {
            return object : Logger<S> {
                override fun log(entry: Entry<S>) {
                    f(entry)
                }
            }
        }

    }

}
