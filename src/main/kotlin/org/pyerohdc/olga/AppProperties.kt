package org.pyerohdc.olga

import java.io.File
import java.io.InputStream
import java.util.*

class AppProperties(private val properties: Properties) {

    companion object {
        const val FILE_NAME = "app-info.properties"
    }

    var appVersion: String?
        get() = properties["app.version"] as? String
        set(value) = run { properties["app.version"] = value }

    var appVersionLatest: String?
        get() = properties["app.version.latest"] as? String
        set(value) = run { properties["app.version.latest"] = value }

    var databaseLocation: String?
        get() = properties["database.location"] as? String
        set(value) = run { properties["database.location"] = value }

    var refresh: Boolean
        get() = (properties["refresh.toggle"] as? String)?.toBoolean() ?: false
        set(value) = run { properties["refresh.toggle"] = value.toString() }

    var refreshDelay: Long
        get() = (properties["refresh.delay"] as? String)?.toLong() ?: 0L
        set(value) = run { properties["refresh.delay"] = value.toString() }

    fun load(inputStream: InputStream) {
        properties.load(inputStream)
    }

    fun writeProps(workFolder: File) {
        workFolder
            .toPath()
            .resolve(FILE_NAME)
            .toFile()
            .outputStream()
            .use { properties.store(it, null) }
    }

}
