package org.pyerohdc.olga.http

import java.time.OffsetDateTime

data class GitlabRelease(
    val name: String,
    val description: String,
    val released_at: OffsetDateTime
)
