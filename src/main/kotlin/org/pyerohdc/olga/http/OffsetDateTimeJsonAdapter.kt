package org.pyerohdc.olga.http

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

class OffsetDateTimeJsonAdapter : JsonAdapter<OffsetDateTime>() {

    @FromJson
    override fun fromJson(reader: JsonReader): OffsetDateTime? {
        val json = reader.nextString()
            ?.replace("Z", "+0000")
        return if (json != null)
            OffsetDateTime.parse(json, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"))
        else null
    }

    @ToJson
    override fun toJson(writer: JsonWriter, value: OffsetDateTime?) {
        throw NotImplementedError("write json OffsetDateTime")
    }

}
