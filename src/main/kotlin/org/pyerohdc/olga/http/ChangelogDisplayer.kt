package org.pyerohdc.olga.http

import com.esotericsoftware.minlog.Log
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import javafx.application.Platform
import javafx.scene.control.Alert
import javafx.scene.layout.Background
import javafx.scene.web.WebView
import javafx.stage.StageStyle
import javafx.stage.Window
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import org.intellij.markdown.flavours.commonmark.CommonMarkFlavourDescriptor
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser
import org.pyerohdc.olga.AppConstants
import org.pyerohdc.olga.Olga
import java.awt.Desktop
import java.io.IOException
import java.net.URI
import java.net.URISyntaxException

fun showChangelog(window: Window) {
    val okHttpClient = OkHttpClient()
    val moshi = Moshi.Builder()
        .add(OffsetDateTimeJsonAdapter())
        .build()

    val request = Request.Builder().url(
        HttpUrl.Builder()
            .scheme("https")
            .host("gitlab.com")
            .addPathSegment("api")
            .addPathSegment("v4")
            .addPathSegment("projects")
            .addPathSegment(AppConstants.REPO_ID)
            .addPathSegment("releases")
            .build()
    )
        .build()

    val releases = okHttpClient.newCall(request).execute().use { listReleasesResponse ->
        if (listReleasesResponse.isSuccessful) {
            val json = listReleasesResponse.body!!.string()
            val listType = Types.newParameterizedType(List::class.java, GitlabRelease::class.java)
            val adapter = moshi.adapter<List<GitlabRelease>>(listType)
            adapter.fromJson(json)
        } else null
    }

    if (releases == null) {
        Platform.runLater {
            val alert = Alert(Alert.AlertType.WARNING)
            alert.title = "Changelog"
            alert.contentText = """
                L'application n'a pas pu joindre l'adresse ${request.url.toUrl()}.
                La connexion sera re-tentée au prochain lancement.
                Veuillez vérifier votre connexion internet, ou contacter votre développeur préféré.
                """.trimIndent()
            alert.initOwner(window)
            alert.showAndWait()
        }
    } else {
        val matchingRelease = releases.find { Olga.APP_PROPERTIES.appVersion!!.contains(it.name) }
        if (matchingRelease != null) {
            Olga.APP_PROPERTIES.appVersionLatest = Olga.APP_PROPERTIES.appVersion
            Olga.APP_PROPERTIES.writeProps(Olga.getJarFolderPath().toFile())

            val flavour = CommonMarkFlavourDescriptor()
            val parsedTree = MarkdownParser(flavour).buildMarkdownTreeFromString(matchingRelease.description)
            val html = HtmlGenerator(matchingRelease.description, parsedTree, flavour).generateHtml()
                .replace(Regex("""(href=")%(\d+")"""), """$1${AppConstants.MILESTONES_BASE}$2 target="_blank"""")

            Platform.runLater {
                val webView = WebView()
                webView.engine.loadContent(
                    """
                    <head>
                        <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
                    </head>
                    <div style="font-family: 'Open Sans', sans-serif;">
                    <h1>Version ${matchingRelease.name}</h1>
                    $html
                    </div>
                    """.trimIndent()
                )
                webView.prefWidth = 500.0
                webView.prefHeight = 400.0
                createPopupHandler(webView)

                val alert = Alert(Alert.AlertType.INFORMATION)
                alert.title = "Changelog"
                alert.headerText = null
                alert.dialogPane.content = webView
                alert.dialogPane.background = Background.EMPTY
                alert.initOwner(window)
                alert.initStyle(StageStyle.UTILITY)
                alert.showAndWait()
            }
        } else {
            Log.warn("Changelog", "Utiliseriez-vous une version de développement ?")
        }
    }
}

private fun createPopupHandler(webView: WebView) {
    // intercept target=_blank hyperlinks
    webView.engine.setCreatePopupHandler {
        val o = webView.engine.executeScript(
            """var list = document.querySelectorAll( ':hover' );
                for (i=list.length-1; i>-1; i--) {
                    if ( list.item(i).getAttribute('href') ) {
                        list.item(i).getAttribute('href');
                        break;
                    }
                }
            """.trimIndent()
        )

        try {
            if (o != null) {
                Desktop.getDesktop().browse(URI(o.toString()))
            } else {
                Log.error("WebView", "No result from uri detector: $o")
            }
        } catch (e: IOException) {
            Log.error("WebView", "Unexpected error obtaining uri: $o", e)
        } catch (e: URISyntaxException) {
            Log.error("WebView", "Could not interpret uri: $o", e)
        }

        null
    }
}
