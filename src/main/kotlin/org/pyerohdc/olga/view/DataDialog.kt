package org.pyerohdc.olga.view

import javafx.beans.binding.BooleanExpression

interface DataDialog {

    fun initDialog()

    fun invalidProperty(): BooleanExpression

}
