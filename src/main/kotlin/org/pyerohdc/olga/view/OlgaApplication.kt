package org.pyerohdc.olga.view

import com.esotericsoftware.minlog.Log
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler
import javafx.application.Application
import javafx.application.Platform
import javafx.scene.control.Alert
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.image.Image
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.stage.DirectoryChooser
import javafx.stage.Stage
import javafx.stage.Window
import jiconfont.icons.font_awesome.FontAwesome
import jiconfont.javafx.IconFontFX
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.bus.SwitchScene
import org.pyerohdc.olga.http.showChangelog
import org.pyerohdc.olga.redux.LoadAllData
import org.pyerohdc.olga.redux.ReloadData
import java.io.File
import java.io.PrintWriter
import java.io.StringWriter
import java.util.concurrent.TimeUnit

class OlgaApplication : Application() {

    private val subscriptions = CompositeDisposable()

    companion object {
        @JvmStatic
        fun selectDatabaseLocation(window: Window, blockOnCancel: Boolean = true) {
            val databaseDirectoryChooser = DirectoryChooser()
            databaseDirectoryChooser.title = "Sélection du dossier de la base"
            databaseDirectoryChooser.initialDirectory =
                Olga.APP_PROPERTIES.databaseLocation?.let { File(it) } ?: Olga.getJarFolderPath().toFile()

            var databaseDirectory: File?
            do {
                databaseDirectory = databaseDirectoryChooser.showDialog(window)
                if (blockOnCancel && databaseDirectory == null) {
                    val alert = Alert(Alert.AlertType.WARNING)
                    alert.title = "Sélection du dossier de la base"
                    alert.contentText = "Vous devez choisir un dossier où la base de données se situera."
                    alert.initOwner(window)
                    alert.showAndWait()
                }
            } while (blockOnCancel && databaseDirectory == null)

            if (databaseDirectory != null) {
                Olga.APP_PROPERTIES.databaseLocation = databaseDirectory.toString()
                Olga.APP_PROPERTIES.writeProps(Olga.getJarFolderPath().toFile())
            }
        }
    }

    override fun start(primaryStage: Stage) {
        manageBusEvent(primaryStage)

        IconFontFX.register(FontAwesome.getIconFont())

        Thread.setDefaultUncaughtExceptionHandler { _, e ->
            onError(e, "Olga", primaryStage)
        }

        if (Olga.APP_PROPERTIES.databaseLocation == null) selectDatabaseLocation(primaryStage)
        loadDatabaseConfiguration()

        primaryStage.title = "OLGA"
        primaryStage.isResizable = true
        primaryStage.icons += Image(javaClass.classLoader.getResource("org/pyerohdc/olga/icon_olga.png")?.toString())

        primaryStage.show()

        primaryStage.setOnCloseRequest {
            Log.info("Olga", "Fermeture de l'application")
            subscriptions.dispose()
        }

        if (Olga.APP_PROPERTIES.appVersion != Olga.APP_PROPERTIES.appVersionLatest) {
            Thread {
                showChangelog(primaryStage)
            }.start()
        }

        Olga.BUS.send(SwitchScene(Components.MAIN_VIEW))
    }

    private fun manageBusEvent(primaryStage: Stage) {
        subscriptions.add(
            Olga.BUS
                .asObservable()
                .observeOn(JavaFxScheduler.platform())
                .subscribe(
                    { Olga.BUS.manageBusEvent(it, primaryStage) },
                    { onError(it, "EventBus", primaryStage) }
                )
        )

        subscriptions.add(
            Olga.BUS
                .asObservable()
                .filter { Olga.APP_PROPERTIES.refresh }
                .throttleFirst(Olga.APP_PROPERTIES.refreshDelay, TimeUnit.MINUTES)
                .subscribe { Olga.STORE.dispatch(ReloadData) }
        )
    }

    private fun loadDatabaseConfiguration() {
        Olga.reloadDatabaseConfiguration()
        Olga.STORE.dispatch(LoadAllData)
    }

    private fun onError(e: Throwable, logCategory: String, primaryStage: Stage) {
        Log.error(logCategory, "Erreur dans l'application", e)
        showErrorDialog(primaryStage, e)
        Platform.exit()
    }

    private fun showErrorDialog(stage: Stage, e: Throwable) {
        val alert = Alert(Alert.AlertType.ERROR)
        alert.initOwner(stage)
        alert.title = "Erreur de l'application"
        alert.headerText =
            "Une erreur est survenue dans l'application, veuillez en informer votre développeur préféré.\n" +
                    "L'application se fermera après la fermeture de cette popup."
        alert.contentText = e.message

        // Create expandable Exception.
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        e.printStackTrace(pw)
        val exceptionText = sw.toString()

        val label = Label("La stacktrace de l'exception est :")

        val textArea = TextArea(exceptionText)
        textArea.isEditable = false
        textArea.isWrapText = true

        textArea.maxWidth = Double.MAX_VALUE
        textArea.maxHeight = Double.MAX_VALUE
        GridPane.setVgrow(textArea, Priority.ALWAYS)
        GridPane.setHgrow(textArea, Priority.ALWAYS)

        val expContent = GridPane()
        expContent.maxWidth = Double.MAX_VALUE
        expContent.add(label, 0, 0)
        expContent.add(textArea, 0, 1)

        // Set expandable Exception into the dialog pane.
        alert.dialogPane.expandableContent = expContent

        alert.showAndWait()
    }

}
