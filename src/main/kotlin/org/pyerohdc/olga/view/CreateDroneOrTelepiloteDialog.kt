package org.pyerohdc.olga.view

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import arrow.core.left
import arrow.core.right
import javafx.beans.binding.Bindings
import javafx.beans.binding.BooleanExpression
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Label
import javafx.scene.control.Spinner
import javafx.scene.control.SpinnerValueFactory
import javafx.scene.control.TextField
import javafx.scene.control.TextFormatter
import javafx.scene.layout.GridPane
import javafx.util.StringConverter
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.Telepilote
import java.net.URL
import java.util.*

class CreateDroneOrTelepiloteDialog : FXMLController, Initializable, DataDialog {

    @FXML
    private lateinit var grid: GridPane

    @FXML
    private lateinit var label1: Label

    @FXML
    private lateinit var label2: Label

    @FXML
    private lateinit var labelChargesDrone: Label

    @FXML
    private lateinit var field1: TextField

    @FXML
    private lateinit var field2: TextField

    @FXML
    private lateinit var chargesDrone: Spinner<Int>

    private var isEdit = false

    private lateinit var data: Either<Telepilote, Drone>

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        chargesDrone.valueFactory = SpinnerValueFactory.IntegerSpinnerValueFactory(0, Int.MAX_VALUE, 0)

        chargesDrone.setOnScroll {
            if (it.deltaY > 0) chargesDrone.increment()
            else chargesDrone.decrement()
        }
    }

    fun initForm(type: Either<Class<Telepilote>, Class<Drone>>) {
        label1.text = "Nom"
        when (type) {
            is Left -> {
                label2.text = "Prénom"

                field1.textFormatter = TextFormatter(object : StringConverter<String>() {
                    override fun toString(obj: String?) = obj?.toUpperCase() ?: ""
                    override fun fromString(string: String?) = toString(string)
                })

                field2.textFormatter = TextFormatter(object : StringConverter<String>() {
                    override fun toString(obj: String?) = obj?.capitalize() ?: ""
                    override fun fromString(string: String?) = toString(string)
                })

                grid.children.removeAll(labelChargesDrone, chargesDrone)
                grid.rowConstraints.removeAt(2)
            }
            is Right -> {
                label2.text = "Matricule"
            }
        }
    }

    fun initData(data: Either<Telepilote, Drone>) {
        isEdit = true
        this.data = data

        initForm(
            when (data) {
                is Left -> data.a.javaClass.left()
                is Right -> data.b.javaClass.right()
            }
        )

        when (data) {
            is Left -> {
                val telepilote = data.a
                field1.text = telepilote.nom
                field2.text = telepilote.prenom
            }
            is Right -> {
                val drone = data.b
                field1.text = drone.nom
                field2.text = drone.matricule
                chargesDrone.valueFactory.value = drone.nombreCharges
            }
        }
    }

    fun getData(type: Either<Class<Telepilote>, Class<Drone>>): Either<Telepilote, Drone> {
        return if (isEdit) {
            return when (val itData = data) {
                is Left -> {
                    val copy = itData.a.copy()
                    copy.nom = field1.text
                    copy.prenom = field2.text
                    copy.left()
                }
                is Right -> {
                    val copy = itData.b.copy()
                    copy.nom = field1.text
                    copy.matricule = field2.text
                    copy.nombreCharges = chargesDrone.value
                    copy.right()
                }
            }
        } else {
            when (type) {
                is Left -> Telepilote {
                    nom = field1.text
                    prenom = field2.text
                    actif = true
                }.left()
                is Right -> Drone {
                    nom = field1.text
                    matricule = field2.text
                    nombreCharges = chargesDrone.value
                    actif = true
                }.right()
            }
        }
    }

    override fun invalidProperty(): BooleanExpression = Bindings.or(
        field1.textProperty().isEmpty,
        field2.textProperty().isEmpty
    )

    override fun initDialog() = field1.requestFocus()
}
