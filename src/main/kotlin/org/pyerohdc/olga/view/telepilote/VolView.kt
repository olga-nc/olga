package org.pyerohdc.olga.view.telepilote

import io.reactivex.rxjavafx.observables.JavaFxObservable
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Control
import javafx.scene.control.TableCell
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.layout.GridPane
import javafx.scene.paint.Color
import javafx.scene.paint.CycleMethod
import javafx.scene.paint.LinearGradient
import javafx.scene.paint.Stop
import javafx.scene.text.Text
import jiconfont.icons.font_awesome.FontAwesome
import org.fxmisc.easybind.EasyBind
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.entities.Vol
import org.pyerohdc.olga.ext.format
import org.pyerohdc.olga.redux.Add
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.WithSubscriptions
import org.pyerohdc.olga.view.util.DialogUtil
import org.pyerohdc.olga.view.util.getIconNode
import org.pyerohdc.olga.view.util.injectContextMenu
import org.pyerohdc.olga.view.util.resizeColumnsToFitContent
import java.net.URL
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class VolView : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var createVol: Button

    @FXML
    private lateinit var volTable: TableView<VolPropertified>

    @FXML
    private lateinit var volBatterieColumn: TableColumn<VolPropertified, String>

    @FXML
    private lateinit var volDureeColumn: TableColumn<VolPropertified, String>

    @FXML
    private lateinit var volDateColumn: TableColumn<VolPropertified, LocalDateTime>

    @FXML
    private lateinit var volChantierColumn: TableColumn<VolPropertified, String>

    @FXML
    private lateinit var volClientColumn: TableColumn<VolPropertified, String>

    @FXML
    private lateinit var volIncidentColumn: TableColumn<VolPropertified, String>

    @FXML
    private lateinit var volCommentaireColumn: TableColumn<VolPropertified, String>

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        volBatterieColumn.setCellValueFactory { EasyBind.map(it.value.batterieDrone) { bd -> "#${bd.batterie.numero} (${bd.drone.getNomMatricule()})" } }
        volDureeColumn.setCellValueFactory { EasyBind.map(it.value.duree, Duration::format) }
        volDateColumn.setCellValueFactory { it.value.date }
        volDateColumn.setCellFactory {
            object : TableCell<VolPropertified, LocalDateTime>() {
                override fun updateItem(item: LocalDateTime?, empty: Boolean) {
                    super.updateItem(item, empty)
                    text =
                        if (item != null && !empty) item.format(DateTimeFormatter.ofPattern("dd MMMM yyyy"))
                        else ""
                }
            }
        }
        volChantierColumn.setCellValueFactory { it.value.chantier }
        volClientColumn.setCellValueFactory { it.value.client }
        volIncidentColumn.setCellValueFactory { it.value.incident }
        volCommentaireColumn.setCellValueFactory { it.value.commentaire }
        volIncidentColumn.setCellFactory { tableCellFactory(it) }
        volCommentaireColumn.setCellFactory { tableCellFactory(it) }

        volTable.setRowFactory { table ->
            val row = TableRow<VolPropertified>()

            JavaFxObservable.valuesOf(row.itemProperty())
                .take(1)
                .subscribe {
                    val originalData = it.originalData
                    injectContextMenu(
                        table,
                        row,
                        "Suppression d'un vol",
                        "Êtes-vous sûr de vouloir supprimer " +
                                "le vol du ${originalData.date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG))}, " +
                                "effectué avec la batterie #${originalData.batterie.numero} " +
                                "du drone ${originalData.batterie.drone.getNomMatricule()} ?",
                        originalData
                    )
                }

            row
        }

        createVol.graphic = getIconNode(
            iconCode = FontAwesome.PAPER_PLANE,
            // De la terre au ciel
            fillColor = LinearGradient(
                0.0,
                1.0,
                1.0,
                0.0,
                true,
                CycleMethod.NO_CYCLE,
                Stop(0.1, Color.FORESTGREEN),
                Stop(0.6, Color.ALICEBLUE),
                Stop(1.0, Color.DEEPSKYBLUE)
            ),
            stroke = Color.LIGHTCYAN.darker(),
            size = createVol.prefHeight * (3.0 / 4.0)
        )
    }

    fun initData(telepilote: Telepilote) {
        initStoreSubscription()

        subscriptions += storeSubject
            .map { it.vols.filter { vol -> vol.telepilote.id == telepilote.id } }
            .distinctUntilChanged()
            .map { it.sortedByDescending { v -> v.date } }
            .subscribe { vols -> volTable.items.setAll(vols.map { VolPropertified(it) }) }

        createVol.setOnMouseClicked { showCreateDialog(telepilote) }

        volTable.resizeColumnsToFitContent()
    }

    private fun showCreateDialog(telepilote: Telepilote) {
        DialogUtil.showCreateDialog<GridPane, CreateVolDialog, Vol>(
            util = DialogUtil(Components.CREATE_VOL_DIALOG, createVol.scene.window, "Création d'un vol"),
            resultConverter = { buttonType, controller -> if (buttonType == ButtonType.OK) controller.getData(telepilote) else null },
            onResultPresent = { Olga.STORE.dispatch(Add(it)) }
        )
    }

    private fun tableCellFactory(tableColumn: TableColumn<VolPropertified, String>): TableCell<VolPropertified, String> {
        val cell = TableCell<VolPropertified, String>()
        val text = Text()
        cell.graphic = text
        cell.prefHeight = Control.USE_COMPUTED_SIZE
        text.wrappingWidthProperty().bind(tableColumn.widthProperty())
        text.textProperty().bind(cell.itemProperty())
        return cell
    }

    private class VolPropertified(
        val batterieDrone: ObjectProperty<BatterieDrone>,
        val duree: ObjectProperty<Duration>,
        val date: ObjectProperty<LocalDateTime>,
        val chantier: StringProperty,
        val client: StringProperty,
        val incident: StringProperty,
        val commentaire: StringProperty,
        val originalData: Vol
    ) {
        constructor(vol: Vol) : this(
            SimpleObjectProperty(BatterieDrone(vol.batterie)),
            SimpleObjectProperty(Duration.ofMinutes(vol.duree.toLong())),
            SimpleObjectProperty(vol.date),
            SimpleStringProperty(vol.chantier),
            SimpleStringProperty(vol.client),
            SimpleStringProperty(vol.incident ?: ""),
            SimpleStringProperty(vol.commentaire ?: ""),
            vol
        )
    }

    private class BatterieDrone(
        val batterie: Batterie,
        val drone: Drone = batterie.drone
    ) : Comparable<BatterieDrone> {
        override fun compareTo(other: BatterieDrone) =
            Comparator.comparingInt<BatterieDrone> { it.batterie.drone.id }
                .thenComparingInt { it.batterie.numero }
                .compare(this, other)
    }

}
