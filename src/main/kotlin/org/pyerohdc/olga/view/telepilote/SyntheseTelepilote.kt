package org.pyerohdc.olga.view.telepilote

import javafx.fxml.FXML
import javafx.scene.control.Label
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.ext.format
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.WithSubscriptions
import java.time.Duration

class SyntheseTelepilote : FXMLController, WithSubscriptions() {

    @FXML
    private lateinit var dureeVolTelepilote: Label

    fun initData(telepilote: Telepilote) {
        initStoreSubscription()

        subscriptions += storeSubject
            .map { it.vols.filter { vol -> vol.telepilote.id == telepilote.id } }
            .distinctUntilChanged()
            .subscribe { vols ->
                dureeVolTelepilote.text = Duration.ofMinutes(vols.sumBy { it.duree }.toLong()).format()
            }
    }

}
