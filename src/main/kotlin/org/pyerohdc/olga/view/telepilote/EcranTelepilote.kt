package org.pyerohdc.olga.view.telepilote

import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Accordion
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.layout.BorderPane
import javafx.scene.layout.GridPane
import javafx.scene.layout.VBox
import org.pyerohdc.olga.bus.SwitchScene
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.WithSubscriptions
import org.pyerohdc.olga.view.util.FXMLLoaderUtil
import org.pyerohdc.olga.view.util.RetourUtil
import org.pyerohdc.olga.view.util.avoidCompleteFold
import java.net.URL
import java.util.*

class EcranTelepilote : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var piloteLabel: Label

    @FXML
    private lateinit var retour: Button

    @FXML
    private lateinit var accordion: Accordion

    @FXML
    private lateinit var volsContainer: BorderPane

    @FXML
    private lateinit var syntheseContainer: BorderPane

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        RetourUtil.initialize(retour) { SwitchScene(Components.MAIN_VIEW) }

        accordion.expandedPane = accordion.panes[0]
        accordion.avoidCompleteFold()
    }

    fun initData(telepilote: Telepilote) {
        initStoreSubscription()

        piloteLabel.text = telepilote.getPrenomNom()

        val volView = FXMLLoaderUtil.load<VBox, VolView>(Components.VOL_VIEW)
        volView.controller.initData(telepilote)
        volsContainer.center = volView.node
        subscribables += volView.controller

        val syntheseTelepilote = FXMLLoaderUtil.load<GridPane, SyntheseTelepilote>(Components.SYNTHESE_TELEPILOTE)
        syntheseTelepilote.controller.initData(telepilote)
        syntheseContainer.center = syntheseTelepilote.node
        subscribables += syntheseTelepilote.controller
    }

}
