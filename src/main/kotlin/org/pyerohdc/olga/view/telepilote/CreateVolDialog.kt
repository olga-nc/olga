package org.pyerohdc.olga.view.telepilote

import javafx.application.Platform
import javafx.beans.binding.Bindings
import javafx.beans.binding.BooleanBinding
import javafx.beans.binding.BooleanExpression
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.DatePicker
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.control.TextFormatter
import javafx.util.converter.IntegerStringConverter
import org.controlsfx.control.textfield.AutoCompletionBinding
import org.controlsfx.control.textfield.TextFields
import org.controlsfx.validation.ValidationResult
import org.controlsfx.validation.ValidationSupport
import org.controlsfx.validation.Validator
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.entities.Vol
import org.pyerohdc.olga.view.DataDialog
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.util.handlePristineStatus
import org.pyerohdc.olga.view.util.isPristine
import java.net.URL
import java.time.LocalDate
import java.time.LocalTime
import java.util.*
import java.util.concurrent.Callable

class CreateVolDialog : FXMLController, Initializable, DataDialog {

    @FXML
    private lateinit var batterie: TextField

    @FXML
    private lateinit var duree: TextField

    @FXML
    private lateinit var date: DatePicker

    @FXML
    private lateinit var chantier: TextField

    @FXML
    private lateinit var client: TextField

    @FXML
    private lateinit var incident: TextArea

    @FXML
    private lateinit var commentaire: TextArea

    private val validationSupport = ValidationSupport()

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        val batteriePredicate: (String) -> (String) -> Boolean =
            { bstr -> { userTextPart -> bstr.contains(userTextPart, ignoreCase = true) } }
        TextFields.bindAutoCompletion(batterie) { param ->
            val userText = (param?.userText ?: "").split(" ")
            Olga.STORE.state.batteries
                .map { batterie -> "#${batterie.numero} (${batterie.drone.getNomMatricule()})" }
                .filter { bstr -> userText.any(batteriePredicate(bstr)) }
                .sortedByDescending { bstr -> userText.count(batteriePredicate(bstr)) }
        }
        batterie.handlePristineStatus()

        duree.textFormatter = TextFormatter<Int>(
            IntegerStringConverter(),
            0
        ) { change -> if (change.controlNewText.matches(Regex("(\\d+)?"))) change else null }
        duree.focusedProperty().addListener { _, _, focus -> if (!focus) duree.commitValue() }
        duree.handlePristineStatus()

        date.value = LocalDate.now()

        val dataLister: (AutoCompletionBinding.ISuggestionRequest?, (Vol) -> String) -> List<String> =
            { param, volMapper ->
                Olga.STORE.state.vols
                    .map { volMapper(it) }
                    .distinct()
                    .filter { it.contains(param?.userText ?: "", ignoreCase = true) }
            }

        TextFields.bindAutoCompletion(chantier) { param -> dataLister(param) { it.chantier } }
        chantier.handlePristineStatus()

        TextFields.bindAutoCompletion(client) { param -> dataLister(param) { it.client } }
        client.handlePristineStatus()

        initValidation()
    }

    private fun initValidation() {
        Platform.runLater {
            validationSupport.registerValidator(
                batterie,
                createPredicateValidator("La batterie doit être sélectionnée par l'auto-complétion") { tf, value ->
                    !tf.isPristine() && findBatterie(value) == null
                })

            validationSupport.registerValidator(
                duree,
                createPredicateValidator("La durée doit être supérieure à 0") { tf, value ->
                    !tf.isPristine()
                            && (value.isBlank() || value == tf.textFormatter.value?.toString())
                            && (tf.textFormatter.value as Int) == 0
                })

            validationSupport.registerValidator(
                date,
                Validator.createPredicateValidator<LocalDate?>({ it != null }, "La date doit être renseignée")
            )

            validationSupport.registerValidator(
                chantier,
                createPredicateValidator("Le chantier doit être renseigné") { tf, value -> !tf.isPristine() && value.isBlank() })

            validationSupport.registerValidator(
                client,
                createPredicateValidator("Le client doit être renseigné") { tf, value -> !tf.isPristine() && value.isBlank() })

            validationSupport.initInitialDecoration()
        }
    }

    private fun createPredicateValidator(
        message: String,
        condition: (TextField, String) -> Boolean
    ) =
        Validator<String> { control, value ->
            ValidationResult.fromErrorIf(
                control,
                message,
                condition(control as TextField, value)
            )
        }

    fun getData(telepilote: Telepilote): Vol {
        return Vol {
            this.telepilote = telepilote
            batterie = findBatterie(this@CreateVolDialog.batterie.text)!!
            duree = this@CreateVolDialog.duree.textFormatter.value as Int
            date = this@CreateVolDialog.date.value.atTime(LocalTime.now())
            chantier = this@CreateVolDialog.chantier.text.trim()
            client = this@CreateVolDialog.client.text.trim()
            incident = this@CreateVolDialog.incident.text.let { if (it?.isBlank() != false) null else it.trim() }
            commentaire = this@CreateVolDialog.commentaire.text.let { if (it?.isBlank() != false) null else it.trim() }
        }
    }

    override fun invalidProperty(): BooleanExpression = listOf(
        booleanBinding(batterie) { batterie.isPristine() },
        booleanBinding(duree) { duree.isPristine() },
        booleanBinding(chantier) { chantier.isPristine() },
        booleanBinding(client) { client.isPristine() })
        .fold(Bindings.or(validationSupport.invalidProperty()!!, booleanBinding { false }), BooleanBinding::or)

    override fun initDialog() = batterie.requestFocus()

    private fun batterieTransformer(batterie: Batterie) = "#${batterie.numero} (${batterie.drone.getNomMatricule()})"

    private fun findBatterie(batterieText: String) =
        Olga.STORE.state.batteries.find { batterieTransformer(it) == batterieText }

    private fun booleanBinding(dependency: TextField? = null, binding: () -> Boolean): BooleanBinding? {
        return if (dependency == null) Bindings.createBooleanBinding(Callable(binding))
        else Bindings.createBooleanBinding(Callable(binding), dependency.textProperty())
    }

}

