package org.pyerohdc.olga.view

import org.pyerohdc.olga.entities.HasIdAndEntity

interface HasOriginalData<T : HasIdAndEntity<T>> {

    val originalData: T

}
