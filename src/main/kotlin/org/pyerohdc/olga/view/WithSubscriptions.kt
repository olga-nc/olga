package org.pyerohdc.olga.view

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler
import io.reactivex.subjects.BehaviorSubject
import org.pyerohdc.olga.Olga

abstract class WithSubscriptions {
    fun disposeSubscriptions() {
        subscriptions.dispose()
        subscribables.forEach { it.disposeSubscriptions() }
    }

    protected val subscriptions = CompositeDisposable()

    protected val storeSubject = BehaviorSubject.createDefault(Olga.STORE.state)

    protected val subscribables = mutableListOf<WithSubscriptions>()

    protected fun initStoreSubscription() {
        subscriptions += Olga.STORE.states
            .observeOn(JavaFxScheduler.platform())
            .subscribe { storeSubject.onNext(it) }
    }

    protected operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
        add(disposable)
    }
}
