package org.pyerohdc.olga.view

import javafx.beans.property.BooleanProperty

interface HasActifProperty {

    val actif: BooleanProperty

}
