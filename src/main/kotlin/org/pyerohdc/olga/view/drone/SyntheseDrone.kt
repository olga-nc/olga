package org.pyerohdc.olga.view.drone

import javafx.beans.property.BooleanProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.control.ButtonBar
import javafx.scene.control.ButtonType
import javafx.scene.control.DatePicker
import javafx.scene.control.Dialog
import javafx.scene.control.Label
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.control.Tooltip
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import jiconfont.icons.font_awesome.FontAwesome
import org.fxmisc.easybind.EasyBind
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.ext.format
import org.pyerohdc.olga.redux.Modify
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.WithSubscriptions
import org.pyerohdc.olga.view.util.getIconNode
import java.net.URL
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class SyntheseDrone : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var dateDerniereMajControleur: Label

    @FXML
    private lateinit var refreshDateDerniereMajControleur: Button

    @FXML
    private lateinit var tempsVolDrone: Label

    @FXML
    private lateinit var tempsVolBatterieTable: TableView<TempsVolByBatterie>

    @FXML
    private lateinit var tempsVolBatterieColumnBatterie: TableColumn<TempsVolByBatterie, Int>

    @FXML
    private lateinit var tempsVolBatterieColumnTempsVol: TableColumn<TempsVolByBatterie, String>

    private class TempsVolByBatterie(
        val numBatterie: ObjectProperty<Int>,
        val tempsVol: ObjectProperty<Duration>,
        val actif: BooleanProperty
    ) {
        constructor(numBatterie: Int, tempsVol: Duration, actif: Boolean) : this(
            SimpleObjectProperty(numBatterie),
            SimpleObjectProperty(tempsVol),
            SimpleBooleanProperty(actif)
        )
    }

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        refreshDateDerniereMajControleur.text = ""
        refreshDateDerniereMajControleur.graphic = getIconNode(
            iconCode = FontAwesome.HISTORY,
            fillColor = Color.GREEN,
            size = refreshDateDerniereMajControleur.prefHeight * (5.0 / 8.0)
        )
        refreshDateDerniereMajControleur.tooltip =
            Tooltip("Mettre à jour la date de dernière mise à jour du contrôleur")

        tempsVolBatterieColumnBatterie.setCellValueFactory { it.value.numBatterie }
        tempsVolBatterieColumnTempsVol.setCellValueFactory { EasyBind.map(it.value.tempsVol) { duration -> duration.format() } }
    }

    fun initData(drone: Drone) {
        initStoreSubscription()

        refreshDateDerniereMajControleur.setOnMouseClicked {
            val dateDerniereMajControleur = showDateMajControleurDialog()
            if (dateDerniereMajControleur != null) {
                val copy = drone.copy()
                copy.dateDerniereMajControleur = dateDerniereMajControleur
                Olga.STORE.dispatch(Modify(drone, copy))
            }
        }

        subscriptions += storeSubject
            .map { it.drones }
            .map { it.first { d -> d.id == drone.id } }
            .distinctUntilChanged()
            .subscribe {
                dateDerniereMajControleur.text =
                    it.dateDerniereMajControleur?.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG))
                        ?: "Jamais"
            }

        // Ici, et uniquement ici, le temps de la batterie est déterminé par lien simple sur l'ID, et pas avec le numéro de série.
        subscriptions += (storeSubject
            .map { it.vols.filter { vol -> vol.batterie.drone.id == drone.id } to it.batteries.filter { batterie -> batterie.drone.id == drone.id } }
            .distinctUntilChanged()
            .subscribe { volsToBatteries ->
                val vols = volsToBatteries.first
                val batteries = volsToBatteries.second
                val dureeVolDrone = Duration.ofMinutes(vols.sumBy { it.duree }.toLong())
                tempsVolDrone.text = dureeVolDrone.format()

                val tempsVolByBatterie = vols.groupBy { it.batterie.numero }
                    .mapValues { it.value.sumBy { vol -> vol.duree } to it.value.first().batterie.actif }
                    .map { TempsVolByBatterie(it.key, Duration.ofMinutes(it.value.first.toLong()), it.value.second) }
                    .let {
                        it.plus(
                            batteries
                                .filter { batterie ->
                                    !it
                                        .map { tv -> tv.numBatterie.get() }
                                        .contains(batterie.numero)
                                }
                                .map { batterie -> TempsVolByBatterie(batterie.numero, Duration.ZERO, batterie.actif) })
                    }
                    .sortedBy { it.numBatterie.value }
                tempsVolBatterieTable.items.setAll(tempsVolByBatterie)
            })

        tempsVolBatterieTable.setRowFactory {
            object : TableRow<TempsVolByBatterie>() {
                override fun updateItem(item: TempsVolByBatterie?, empty: Boolean) {
                    super.updateItem(item, empty)

                    if (item == null || empty) {
                        style = ""
                    } else if (!item.actif.value) {
                        style = "-fx-background-color: #787878;"
                    }
                }
            }
        }
    }

    private fun showDateMajControleurDialog(): LocalDateTime? {
        val dialog = Dialog<LocalDateTime>()
        dialog.title = "Mise à jour de la date de dernière mise à jour du contrôleur"
        dialog.headerText = "Quelle est la date de dernière mise à jour du contrôleur de drone ?"

        val dialogPane = dialog.dialogPane
        dialogPane.buttonTypes.addAll(ButtonType.OK, ButtonType.CANCEL)

        val datePicker = DatePicker(LocalDate.now())
        datePicker.maxWidth = Double.MAX_VALUE
        GridPane.setHgrow(datePicker, Priority.ALWAYS)
        GridPane.setFillWidth(datePicker, true)

        val grid = GridPane()
        grid.hgap = 10.0
        grid.vgap = 10.0
        grid.maxWidth = Double.MAX_VALUE
        grid.alignment = Pos.CENTER_LEFT

        grid.add(datePicker, 0, 0)
        dialogPane.content = grid

        dialog.initOwner(refreshDateDerniereMajControleur.scene.window)

        dialog.setResultConverter {
            if (it?.buttonData == ButtonBar.ButtonData.OK_DONE) datePicker.value.atTime(LocalTime.now()) else null
        }

        return dialog.showAndWait().orElse(null)
    }

}
