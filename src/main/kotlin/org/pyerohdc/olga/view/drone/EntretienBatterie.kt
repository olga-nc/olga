package org.pyerohdc.olga.view.drone

import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.layout.BorderPane
import javafx.scene.paint.Color
import jiconfont.icons.font_awesome.FontAwesome
import org.pyerohdc.olga.AppState
import org.pyerohdc.olga.bus.SwitchScene
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.CarnetChargeBatterie
import org.pyerohdc.olga.entities.CarnetMaintenanceBatterie
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.WithSubscriptions
import org.pyerohdc.olga.view.util.FXMLLoaderUtil
import org.pyerohdc.olga.view.util.RetourUtil
import org.pyerohdc.olga.view.util.getIconNode
import java.net.URL
import java.util.*

class EntretienBatterie : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var batterieLabel: Label

    @FXML
    private lateinit var retour: Button

    @FXML
    private lateinit var santeTab: Tab

    @FXML
    private lateinit var maintenanceTab: Tab

    @FXML
    private lateinit var chargeTab: Tab

    @FXML
    private lateinit var santeContainer: BorderPane

    @FXML
    private lateinit var maintenanceContainer: BorderPane

    @FXML
    private lateinit var chargeContainer: BorderPane

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        santeTab.graphic = getIconNode(
            iconCode = FontAwesome.HEART_O,
            fillColor = Color.DARKRED,
            size = 16
        )

        maintenanceTab.graphic = getIconNode(
            iconCode = FontAwesome.WRENCH,
            fillColor = Color.GREY,
            size = 16
        )

        chargeTab.graphic = getIconNode(
            iconCode = FontAwesome.BATTERY_THREE_QUARTERS,
            fillColor = Color.LIMEGREEN,
            size = 16
        )
    }

    fun initData(batterie: Batterie) {
        initStoreSubscription()

        RetourUtil.initialize(retour) { SwitchScene(Components.ECRAN_DRONE, batterie.drone, batterie) }

        batterieLabel.text = "Batterie #${batterie.numero} du drone ${batterie.drone.getNomMatricule()}"

        val santeView = FXMLLoaderUtil.load<BorderPane, SanteView>(Components.SANTE_VIEW)
        santeView.controller.initData(batterie)
        santeContainer.center = santeView.node
        subscribables += santeView.controller

        val maintenanceView = FXMLLoaderUtil.load<BorderPane, CarnetView>(Components.CARNET_VIEW)
        maintenanceView.controller.initData(
            batterie,
            "Création d'une maintenance de batterie",
            "Nouvelle maintenance",
            FontAwesome.COGS,
            Color.BLACK,
            CarnetMaintenanceBatterie::class,
            AppState::carnetsMaintenanceBatterie
        )
        maintenanceContainer.center = maintenanceView.node
        subscribables += maintenanceView.controller

        val chargeView = FXMLLoaderUtil.load<BorderPane, CarnetView>(Components.CARNET_VIEW)
        chargeView.controller.initData(
            batterie,
            "Création d'un rapport de charge de batterie",
            "Nouvelle charge",
            FontAwesome.PLUG,
            Color.DARKGREEN,
            CarnetChargeBatterie::class,
            AppState::carnetsChargeBatterie
        )
        chargeContainer.center = chargeView.node
        subscribables += chargeView.controller
    }

}
