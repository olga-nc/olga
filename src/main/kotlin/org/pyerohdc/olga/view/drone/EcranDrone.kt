package org.pyerohdc.olga.view.drone

import io.reactivex.rxjavafx.schedulers.JavaFxScheduler
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Accordion
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.layout.BorderPane
import javafx.scene.layout.GridPane
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import jiconfont.icons.font_awesome.FontAwesome
import jiconfont.javafx.IconNode
import jiconfont.javafx.StackedIconNode
import org.controlsfx.control.NotificationPane
import org.controlsfx.control.action.Action
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.bus.SwitchScene
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.CarnetChargeBatterie
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.redux.Add
import org.pyerohdc.olga.redux.Modify
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.WithSubscriptions
import org.pyerohdc.olga.view.util.DialogUtil
import org.pyerohdc.olga.view.util.FXMLLoaderUtil
import org.pyerohdc.olga.view.util.RetourUtil
import org.pyerohdc.olga.view.util.avoidCompleteFold
import org.pyerohdc.olga.view.util.getIconNode
import java.net.URL
import java.util.*
import java.util.concurrent.TimeUnit


class EcranDrone : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var rootContainer: VBox

    @FXML
    private lateinit var droneLabel: Label

    @FXML
    private lateinit var retour: Button

    @FXML
    private lateinit var accordion: Accordion

    @FXML
    private lateinit var syntheseContainer: BorderPane

    @FXML
    private lateinit var maintenancePieceContainer: BorderPane

    @FXML
    private lateinit var syntheseBatterieTabPane: TabPane

    @FXML
    private lateinit var createBatterieButton: Button

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        RetourUtil.initialize(retour) { SwitchScene(Components.MAIN_VIEW) }

        accordion.expandedPane = accordion.panes[0]
        accordion.avoidCompleteFold()

        createBatterieButton.graphic = getIconNode(
            iconCode = FontAwesome.PLUS,
            fillColor = Color.GREEN,
            size = createBatterieButton.prefHeight / 2
        )
    }

    fun initData(drone: Drone, batterie: Batterie? = null) {
        initStoreSubscription()

        droneLabel.text = drone.getNomMatricule()

        val syntheseDrone = FXMLLoaderUtil.load<GridPane, SyntheseDrone>(Components.SYNTHESE_DRONE)
        syntheseDrone.controller.initData(drone)
        syntheseContainer.center = syntheseDrone.node
        subscribables += syntheseDrone.controller

        val maintenancePiece = FXMLLoaderUtil.load<VBox, CarnetView>(Components.CARNET_VIEW)
        maintenancePiece.controller.initData(drone)
        maintenancePieceContainer.center = maintenancePiece.node
        subscribables += maintenancePiece.controller

        subscriptions += storeSubject
            .map { it.batteries.filter { batterie -> batterie.drone.id == drone.id && batterie.actif } }
            .distinctUntilChanged()
            .map { it.sortedBy { batterie -> batterie.numero } }
            .scan(emptyList<Batterie>() to emptyList<Batterie>()) { pair, lb -> pair.second to lb }
            .subscribe { batteriesPair -> onBatteriesChangeFromStore(batteriesPair, drone) }

        createBatterieButton.setOnMouseClicked {
            DialogUtil.showCreateDialog<GridPane, CreateBatterieDialog, Batterie>(
                util = DialogUtil(
                    component = Components.CREATE_BATTERIE_DIALOG,
                    dialogOwner = syntheseBatterieTabPane.scene.window,
                    dialogTitle = "Création d'une batterie"
                ),
                afterCreationControllerInit = { it.init(drone) },
                resultConverter = { buttonType, controller -> if (buttonType == ButtonType.OK) controller.getData() else null },
                onResultPresent = { Olga.STORE.dispatch(Add(it)) }
            )
        }

        if (batterie != null) {
            accordion.expandedPane = accordion.panes.find { it.text == "Batteries" }
            syntheseBatterieTabPane.selectionModel.select(syntheseBatterieTabPane.tabs.find { batterie == it.userData as Batterie })
        }

        notifyOnCycleThresholdReached(drone)
    }

    private fun notifyOnCycleThresholdReached(drone: Drone) {
        val notificationPane = NotificationPane(rootContainer)
        notificationPane.graphic = getNotificationIcon()
        notificationPane.actions += Action("OK") { notificationPane.hide() }
        notificationPane.isShowFromTop = true

        // Hack dégueu qui évite pourtant d'avoir réellement un NotificationPane en noeud racine dans le FXML
        (rootContainer.parent as BorderPane).center = notificationPane

        subscriptions += storeSubject
            // Les batteries associées au drone
            .map {
                it.batteries
                    .filter { batterie -> drone.id == batterie.drone.id }
                    .map { batterie -> batterie.numeroSerie }
            }
            // Les batteries avec le même numéro de série que celles du drone
            .switchMap { numerosSerie ->
                storeSubject
                    .map { state ->
                        state.batteries
                            .filter { batterie -> numerosSerie.contains(batterie.numeroSerie) }
                    }
            }
            // Les carnets de charge associés aux batteries précédemment filtrées
            .switchMap { batteries ->
                val batteriesId = batteries.map { b -> b.id }
                storeSubject
                    .map { state ->
                        state.carnetsChargeBatterie
                            .filter { ccb -> batteriesId.contains(ccb.batterie.id) && (ccb.batterie.dateDernierCycle == null || ccb.date > ccb.batterie.dateDernierCycle) }
                            .map { ccb ->
                                CarnetChargeWithBatterieInfos(ccb, batteries
                                    .filter { batterie -> ccb.batterie.numeroSerie == batterie.numeroSerie }
                                    .map { b -> b.drone.id to b.numero }
                                    .toMap())
                            }
                    }
            }
            .distinctUntilChanged()
            .map {
                it
                    .groupBy { ccbi -> ccbi.ccb.batterie.numeroSerie }
                    .mapValues { e -> e.value.first().droneIdToNumero[drone.id] to e.value.size }
                    .filter { e -> e.value.second >= drone.nombreCharges }
                    .mapKeys { e -> e.value.first }
                    .mapValues { e -> e.value.second }
            }
            .filter { it.isNotEmpty() }
            .delay(1, TimeUnit.SECONDS)
            .observeOn(JavaFxScheduler.platform())
            .subscribe { m ->
                val message =
                    if (m.size == 1) m.entries.first().let { "Cycle de charge recommandé pour la batterie #${it.key} (${it.value} charges / ${drone.nombreCharges})" }
                    else "Cycle de charge recommandé pour les batteries ${m.keys.map { it }.joinToString(", ") { "#$it" }}"

                notificationPane.text = message
                notificationPane.show()
            }

    }

    private fun getNotificationIcon(): StackedIconNode {
        val battery = IconNode(FontAwesome.BATTERY_THREE_QUARTERS)
        battery.fill = Color.LIMEGREEN
        battery.iconSize = 12

        val refresh = IconNode(FontAwesome.REPEAT)
        refresh.fill = Color.valueOf("#CD3232")
        refresh.iconSize = 32

        return StackedIconNode(battery, refresh)
    }

    private fun onBatteriesChangeFromStore(batteriesPair: Pair<List<Batterie>, List<Batterie>>, drone: Drone) {
        syntheseBatterieTabPane.tabs.setAll(
            batteriesPair.second
                .filter { it.drone.id == drone.id }
                .map {
                    val tabName = "#${it.numero}"
                    val tab = syntheseBatterieTabPane.tabs.find { tab ->
                        (tab.userData as Batterie?)?.id?.equals(it.id) ?: false
                    } ?: Tab(tabName)
                    tab.userData = it

                    val content = FXMLLoaderUtil.load<VBox, SyntheseBatterie>(Components.SYNTHESE_BATTERIE)
                    content.controller.initData(it)
                    tab.content = content.node
                    tab.isClosable = true
                    tab.setOnCloseRequest { e ->
                        val alert = Alert(Alert.AlertType.CONFIRMATION)
                        alert.initOwner(syntheseBatterieTabPane.scene.window)
                        alert.title = "Désactivation d'une batterie"
                        alert.contentText = """
                            Êtes-vous sûr de vouloir désactiver la batterie #${it.numero}" ?
                            Cela ne supprimera pas ses données associées.
                        """.trimIndent()

                        val isPresent = alert.showAndWait()
                            .filter { b -> b == ButtonType.OK }
                            .isPresent

                        if (isPresent) {
                            val copy = it.copy()
                            copy.actif = false
                            Olga.STORE.dispatch(Modify(it, copy))
                        } else {
                            e.consume()
                        }
                    }
                    tab
                }
        )

        val diff =
            ((batteriesPair.second - batteriesPair.first) + (batteriesPair.first - batteriesPair.second)).distinct()
        if (diff != batteriesPair.second) {
            val batterie = diff.first()
            syntheseBatterieTabPane.selectionModel.select(syntheseBatterieTabPane.tabs.find { it.userData == batterie }
                ?: syntheseBatterieTabPane.tabs.first())
        }
    }

    private class CarnetChargeWithBatterieInfos(
        val ccb: CarnetChargeBatterie,
        val droneIdToNumero: Map<Int, Int>
    )

}
