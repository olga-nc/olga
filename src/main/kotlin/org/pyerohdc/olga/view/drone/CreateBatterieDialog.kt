package org.pyerohdc.olga.view.drone

import javafx.application.Platform
import javafx.beans.binding.Bindings
import javafx.beans.binding.BooleanBinding
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Spinner
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory
import javafx.scene.control.TextField
import org.controlsfx.validation.ValidationResult
import org.controlsfx.validation.ValidationSupport
import org.controlsfx.validation.Validator
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.view.DataDialog
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.util.handlePristineStatus
import org.pyerohdc.olga.view.util.isPristine
import java.net.URL
import java.util.*
import java.util.concurrent.Callable

class CreateBatterieDialog : FXMLController, Initializable, DataDialog {

    @FXML
    private lateinit var numero: Spinner<Int>

    @FXML
    private lateinit var numeroSerie: TextField

    private val validationSupport = ValidationSupport()

    private lateinit var drone: Drone

    private var batterie: Batterie? = null

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        numero.valueFactory = IntegerSpinnerValueFactory(0, Int.MAX_VALUE, 0)

        numero.setOnScroll {
            if (it.deltaY > 0) numero.increment()
            else numero.decrement()
        }

        numeroSerie.handlePristineStatus()
    }

    fun init(drone: Drone, batterie: Batterie? = null) {
        val numeroBatterie = batterie?.numero
        val numeros = Olga.STORE.state.batteries
            .filter { it.drone.id == drone.id && (numeroBatterie == null || it.numero != numeroBatterie) }
            .map { it.numero }

        // Obligé de faire ça : https://bitbucket.org/controlsfx/controlsfx/issues/539
        Platform.runLater {
            validationSupport.registerValidator(numero, Validator.createPredicateValidator<Int>({
                !numeros.contains(it)
            }, "Le numéro de la batterie ne doit pas déjà exister pour le drone"))

            validationSupport.registerValidator(
                numeroSerie,
                Validator<String> { t, u ->
                    t as TextField
                    ValidationResult.fromErrorIf(
                        t,
                        "Le numéro de série doit être renseigné",
                        !t.isPristine() && u.isNullOrBlank()
                    )
                }
            )

            validationSupport.initInitialDecoration()
        }

        this.drone = drone
        this.batterie = batterie

        if (batterie != null) {
            numero.valueFactory.value = batterie.numero
            numeroSerie.text = batterie.numeroSerie
        }
    }

    override fun invalidProperty(): BooleanBinding = Bindings.or(
        validationSupport.invalidProperty()!!,
        Bindings.createBooleanBinding(Callable { numeroSerie.isPristine() }, numeroSerie.textProperty())
    )

    override fun initDialog() {
        numero.requestFocus()
    }

    fun getData(): Batterie {
        val innerBatterie = batterie
        return if (innerBatterie != null) {
            val copy = innerBatterie.copy()
            copy.numero = numero.value
            copy.numeroSerie = numeroSerie.text
            copy
        } else {
            Batterie {
                drone = this@CreateBatterieDialog.drone
                numero = this@CreateBatterieDialog.numero.value
                numeroSerie = this@CreateBatterieDialog.numeroSerie.text
            }
        }
    }

}
