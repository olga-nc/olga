package org.pyerohdc.olga.view.drone

import io.reactivex.rxjavafx.observables.JavaFxObservable
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.geometry.Insets
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Control
import javafx.scene.control.TableCell
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.layout.BorderPane
import javafx.scene.layout.GridPane
import javafx.scene.paint.Color
import javafx.scene.text.Text
import jiconfont.IconCode
import jiconfont.icons.font_awesome.FontAwesome
import me.liuwj.ktorm.entity.Entity
import org.pyerohdc.olga.AppState
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.Carnet
import org.pyerohdc.olga.entities.CarnetBatterie
import org.pyerohdc.olga.entities.CarnetChargeBatterie
import org.pyerohdc.olga.entities.CarnetMaintenanceBatterie
import org.pyerohdc.olga.entities.CarnetMaintenancePiece
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.HasIdAndEntity
import org.pyerohdc.olga.ext.formatLocalizedFullDateTime
import org.pyerohdc.olga.redux.Add
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.WithSubscriptions
import org.pyerohdc.olga.view.util.DialogUtil
import org.pyerohdc.olga.view.util.getIconNode
import org.pyerohdc.olga.view.util.injectContextMenu
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*
import kotlin.reflect.KClass

class CarnetView : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var headerContainer: BorderPane

    @FXML
    private lateinit var createCarnet: Button

    @FXML
    private lateinit var carnetMaintenanceTable: TableView<CarnetMaintenance>

    @FXML
    private lateinit var carnetMaintenanceDateColumn: TableColumn<CarnetMaintenance, LocalDateTime>

    @FXML
    private lateinit var carnetMaintenanceTelepiloteColumn: TableColumn<CarnetMaintenance, String>

    @FXML
    private lateinit var carnetMaintenanceNotesColumn: TableColumn<CarnetMaintenance, String>

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        createCarnet.graphic = getIconNode(
            iconCode = FontAwesome.COGS,
            size = createCarnet.prefHeight * (5.0 / 8.0)
        )

        carnetMaintenanceDateColumn.setCellValueFactory { it.value.date }
        carnetMaintenanceDateColumn.setCellFactory {
            object : TableCell<CarnetMaintenance, LocalDateTime>() {
                override fun updateItem(item: LocalDateTime?, empty: Boolean) {
                    super.updateItem(item, empty)
                    text =
                        if (item != null && !empty) item.formatLocalizedFullDateTime()
                        else ""
                }
            }

        }
        carnetMaintenanceTelepiloteColumn.setCellValueFactory { it.value.telepilote }
        carnetMaintenanceNotesColumn.setCellValueFactory { it.value.notes }
        carnetMaintenanceNotesColumn.setCellFactory { tableColumn ->
            val cell = TableCell<CarnetMaintenance, String>()
            val text = Text()
            cell.graphic = text
            cell.prefHeight = Control.USE_COMPUTED_SIZE
            text.wrappingWidthProperty().bind(tableColumn.widthProperty())
            text.textProperty().bind(cell.itemProperty())
            cell
        }

        carnetMaintenanceTable.setRowFactory { table ->
            val row = TableRow<CarnetMaintenance>()

            fun <T> injectContextMenu(carnetType: String, dataToDelete: T)
                    where T : HasIdAndEntity<T>,
                          T : Carnet {
                injectContextMenu(
                    table,
                    row,
                    "Suppression d'un $carnetType",
                    "Êtes-vous sûr de vouloir supprimer l'entrée du ${dataToDelete.date.format(
                        DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
                    )} effectuée par ${dataToDelete.telepilote.getPrenomNom()} ?",
                    dataToDelete
                )
            }

            JavaFxObservable.valuesOf(row.itemProperty())
                .take(1)
                .subscribe {
                    when (val originalData = row.item.originalData) {
                        is CarnetMaintenancePiece -> injectContextMenu("carnet de maintenance de pièce", originalData)
                        is CarnetChargeBatterie -> injectContextMenu("carnet de charge de batterie", originalData)
                        is CarnetMaintenanceBatterie -> injectContextMenu(
                            "carnet de maintenance de batterie",
                            originalData
                        )
                        else -> error("Type non casté pour la table de données : ${originalData.javaClass}")
                    }
                }

            row
        }
    }

    private fun <T> initTableSubscription(storeProjection: (AppState) -> List<T>) where T : Carnet, T : HasIdAndEntity<T> {
        subscriptions += storeSubject
            .map { storeProjection(it) }
            .distinctUntilChanged()
            .subscribe { l ->
                carnetMaintenanceTable.items.setAll(l.map { CarnetMaintenance(it) })
            }
    }

    fun initData(drone: Drone) {
        initStoreSubscription()
        initTableSubscription { it.carnetsMaintenancePiece.filter { mp -> mp.drone.id == drone.id } }
        createCarnet.setOnMouseClicked { showCreateDialog(drone) }
    }

    fun <T> initData(
        batterie: Batterie,
        createTitle: String,
        createButtonText: String,
        buttonIcon: IconCode,
        buttonIconColor: Color,
        c: KClass<T>,
        storeProjection: (AppState) -> List<T>
    ) where T : CarnetBatterie,
            T : HasIdAndEntity<T> {
        initStoreSubscription()
        initTableSubscription { storeProjection(it).filter { mp -> mp.batterie.numeroSerie == batterie.numeroSerie } }

        createCarnet.setOnMouseClicked { showCreateDialog(createTitle, batterie, c) }
        createCarnet.text = createButtonText
        createCarnet.graphic = getIconNode(
            iconCode = buttonIcon,
            fillColor = buttonIconColor,
            size = createCarnet.prefHeight * (5.0 / 8.0)
        )

        // Dans le cas de la gestion d'un carnet de batterie, on n'affiche pas de titre,
        // et on retravaille un peu les marges internes, parce que les TitledPane font de la merde avec ça...
        headerContainer.left = null
        headerContainer.padding = Insets(5.0, 5.0, 0.0, 0.0)
    }

    private fun showCreateDialog(drone: Drone) {
        showCreateDialog("Création d'une maintenance de pièce") { buttonType, controller ->
            if (buttonType == ButtonType.OK)
                controller.getData().let {
                    CarnetMaintenancePiece {
                        date = it.date
                        notes = it.notes
                        telepilote = it.telepilote
                        this.drone = drone
                    }
                }
            else null
        }
    }

    private fun <T> showCreateDialog(title: String, batterie: Batterie, c: KClass<T>)
            where T : CarnetBatterie,
                  T : HasIdAndEntity<T> {
        showCreateDialog(title) { buttonType, controller ->
            if (buttonType == ButtonType.OK) controller.getData().toCarnetBatterie(batterie, c)
            else null
        }
    }

    private fun <T : HasIdAndEntity<T>> showCreateDialog(
        title: String,
        resultConverter: (ButtonType, CreateCarnetDialog) -> T?
    ) {
        DialogUtil.showCreateDialog<GridPane, CreateCarnetDialog, T>(
            util = DialogUtil(
                component = Components.CREATE_CARNET_DIALOG,
                dialogOwner = createCarnet.scene.window,
                dialogTitle = title
            ),
            resultConverter = resultConverter,
            onResultPresent = { Olga.STORE.dispatch(Add(it)) }
        )
    }

    private fun <T> Carnet.toCarnetBatterie(batterie: Batterie, c: KClass<T>): T
            where T : CarnetBatterie,
                  T : HasIdAndEntity<T> {
        @Suppress("UNCHECKED_CAST") val entity = Entity.create(c) as T
        entity.date = this.date
        entity.notes = this.notes
        entity.telepilote = this.telepilote
        entity.batterie = batterie

        return entity
    }

    private class CarnetMaintenance(
        val date: ObjectProperty<LocalDateTime>,
        val telepilote: StringProperty,
        val notes: StringProperty,
        val originalData: Carnet
    ) {
        constructor(carnet: Carnet) : this(
            SimpleObjectProperty(carnet.date),
            SimpleStringProperty(carnet.telepilote.getNomPrenom()),
            SimpleStringProperty(carnet.notes ?: ""),
            carnet
        )
    }

}
