package org.pyerohdc.olga.view.drone

import javafx.beans.binding.Bindings
import javafx.beans.binding.BooleanExpression
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.DatePicker
import javafx.scene.control.Label
import javafx.scene.control.ToggleButton
import org.controlsfx.control.SegmentedButton
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.Sante
import org.pyerohdc.olga.entities.SanteValue
import org.pyerohdc.olga.ext.extractHexString
import org.pyerohdc.olga.view.DataDialog
import org.pyerohdc.olga.view.FXMLController
import java.net.URL
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

class CreateSanteDialog : FXMLController, Initializable, DataDialog {

    @FXML
    lateinit var date: DatePicker

    @FXML
    lateinit var sante: SegmentedButton

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        date.value = LocalDate.now()

        val santeValues = SanteValue.values().size
        sante.styleClass += SegmentedButton.STYLE_CLASS_DARK
        sante.buttons += SanteValue.values()
            .map {
                val colorLabel = Label()
                colorLabel.id = "sante-label"
                colorLabel.stylesheets += "/org/pyerohdc/olga/view/drone/synthese-batterie.css"
                colorLabel.style = "-fx-background-color: #${it.couleur.extractHexString()};"
                colorLabel.minWidth = 32.0
                colorLabel.maxWidth = Double.MAX_VALUE

                val toggle = ToggleButton("", colorLabel)
                toggle.userData = it
                toggle.maxWidth = Double.MAX_VALUE
                toggle.prefWidthProperty().bind(sante.widthProperty().divide(santeValues))
                toggle
            }
    }

    override fun invalidProperty(): BooleanExpression = Bindings.or(
        date.valueProperty().isNull,
        sante.toggleGroup.selectedToggleProperty().isNull
    )

    override fun initDialog() = date.requestFocus()

    fun getData(batterie: Batterie): Sante {
        return Sante {
            this.batterie = batterie
            date = this@CreateSanteDialog.date.value.atTime(LocalTime.now())
            pourcentage = (sante.toggleGroup.selectedToggle.userData as SanteValue).pourcentage
        }
    }

}
