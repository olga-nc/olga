package org.pyerohdc.olga.view.drone

import io.reactivex.rxjavafx.observables.JavaFxObservable
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Control
import javafx.scene.control.Label
import javafx.scene.control.TableCell
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.layout.GridPane
import javafx.scene.layout.Region
import javafx.scene.paint.Color
import jiconfont.icons.font_awesome.FontAwesome
import org.fxmisc.easybind.EasyBind
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.Sante
import org.pyerohdc.olga.entities.SanteValue
import org.pyerohdc.olga.ext.extractHexString
import org.pyerohdc.olga.ext.formatLocalizedFullDateTime
import org.pyerohdc.olga.redux.Add
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.WithSubscriptions
import org.pyerohdc.olga.view.util.DialogUtil
import org.pyerohdc.olga.view.util.getIconNode
import org.pyerohdc.olga.view.util.injectContextMenu
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class SanteView : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var createSante: Button

    @FXML
    private lateinit var santeTable: TableView<SantePropertified>

    @FXML
    private lateinit var santeDateColumn: TableColumn<SantePropertified, LocalDateTime>

    @FXML
    private lateinit var santeCouleurColumn: TableColumn<SantePropertified, Int>

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        santeDateColumn.setCellValueFactory { it.value.date }
        santeDateColumn.setCellFactory {
            object : TableCell<SantePropertified, LocalDateTime>() {
                override fun updateItem(item: LocalDateTime?, empty: Boolean) {
                    super.updateItem(item, empty)
                    text =
                        if (item != null && !empty) item.formatLocalizedFullDateTime()
                        else ""
                }
            }
        }
        santeCouleurColumn.setCellValueFactory { it.value.couleur }
        santeCouleurColumn.setCellFactory {
            val cell = TableCell<SantePropertified, Int>()
            val colorLabel = Label()
            colorLabel.id = "sante-label"
            colorLabel.stylesheets += "/org/pyerohdc/olga/view/drone/synthese-batterie.css"
            colorLabel.styleProperty().bind(EasyBind
                .map(cell.itemProperty()) { it }
                .filter { it != null }
                .map { SanteValue.fromPourcentage(it) }
                .map { "-fx-background-color: #${it.couleur.extractHexString()};" }
                .orElse(""))
            colorLabel.minHeight = Region.USE_PREF_SIZE
            colorLabel.prefHeight = 32.0
            colorLabel.maxWidth = Double.MAX_VALUE
            colorLabel.maxHeight = Region.USE_PREF_SIZE

            cell.graphicProperty().bind(EasyBind.map(cell.itemProperty()) { if (it != null) colorLabel else null })
            cell.prefHeight = Control.USE_COMPUTED_SIZE
            cell
        }

        santeTable.setRowFactory { table ->
            val row = TableRow<SantePropertified>()

            JavaFxObservable.valuesOf(row.itemProperty())
                .take(1)
                .subscribe {
                    val originalData = it.originalData
                    injectContextMenu(
                        table,
                        row,
                        "Suppression d'une santé de batterie",
                        "Êtes-vous sûr de vouloir supprimer l'entrée du ${originalData.date.format(
                            DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
                        )}, effectuée à ${originalData.date.toLocalTime().format(DateTimeFormatter.ISO_LOCAL_TIME)} ?",
                        originalData
                    )
                }

            row
        }

        createSante.graphic = getIconNode(
            iconCode = FontAwesome.HEARTBEAT,
            fillColor = Color.DARKRED,
            size = createSante.prefHeight * (5.0 / 8.0)
        )
    }

    fun initData(batterie: Batterie) {
        initStoreSubscription()

        subscriptions += storeSubject
            .map { it.santes.filter { sante -> sante.batterie.numeroSerie == batterie.numeroSerie } }
            .distinctUntilChanged()
            .map { it.sortedByDescending { s -> s.date } }
            .subscribe { santes -> santeTable.items.setAll(santes.map { SantePropertified(it) }) }

        createSante.setOnMouseClicked { showCreateDialog(batterie) }
    }

    private fun showCreateDialog(batterie: Batterie) {
        DialogUtil.showCreateDialog<GridPane, CreateSanteDialog, Sante>(
            util = DialogUtil(
                component = Components.CREATE_SANTE_DIALOG,
                dialogOwner = createSante.scene.window,
                dialogTitle = "Création d'un relevé de santé de la batterie"
            ),
            resultConverter = { buttonType, controller -> if (buttonType == ButtonType.OK) controller.getData(batterie) else null },
            onResultPresent = { Olga.STORE.dispatch(Add(it)) }
        )
    }

    private class SantePropertified(
        val date: ObjectProperty<LocalDateTime>,
        val couleur: ObjectProperty<Int>,
        val originalData: Sante
    ) {
        constructor(sante: Sante) : this(
            SimpleObjectProperty(sante.date),
            SimpleObjectProperty(sante.pourcentage),
            sante
        )
    }

}
