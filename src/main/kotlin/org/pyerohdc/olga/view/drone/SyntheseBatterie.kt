package org.pyerohdc.olga.view.drone

import arrow.core.toOption
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Label
import javafx.scene.control.TextInputDialog
import javafx.scene.control.Tooltip
import javafx.scene.layout.GridPane
import javafx.scene.paint.Color
import jiconfont.icons.font_awesome.FontAwesome
import org.controlsfx.control.textfield.AutoCompletionBinding
import org.controlsfx.control.textfield.TextFields
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.bus.SwitchScene
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.CarnetMaintenanceBatterie
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.ext.extractHexString
import org.pyerohdc.olga.redux.Add
import org.pyerohdc.olga.redux.Modify
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.WithSubscriptions
import org.pyerohdc.olga.view.util.DialogUtil
import org.pyerohdc.olga.view.util.getIconNode
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class SyntheseBatterie : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var grid: GridPane

    @FXML
    private lateinit var modify: Button

    @FXML
    private lateinit var numBatterie: Label

    @FXML
    private lateinit var numSerie: Label

    @FXML
    private lateinit var dateDernierCycle: Label

    @FXML
    private lateinit var updateDateDernierCycle: Button

    @FXML
    private lateinit var sante: Label

    @FXML
    private lateinit var entretienButton: Button

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        sante.text = ""

        updateDateDernierCycle.text = ""
        updateDateDernierCycle.graphic = getIconNode(
            iconCode = FontAwesome.HISTORY,
            fillColor = Color.GREEN,
            size = updateDateDernierCycle.prefHeight * (5.0 / 8.0)
        )
        updateDateDernierCycle.tooltip = Tooltip("Mettre à jour la date du dernier cycle de batterie")

        entretienButton.graphic = getIconNode(
            iconCode = FontAwesome.WRENCH,
            fillColor = Color.GREY,
            size = entretienButton.minHeight / 2
        )

        modify.text = ""
        modify.graphic = getIconNode(
            iconCode = FontAwesome.PENCIL,
            fillColor = Color.CORNFLOWERBLUE,
            size = modify.prefHeight / 2
        )
    }

    fun initData(batterie: Batterie) {
        initStoreSubscription()

        numBatterie.text = batterie.numero.toString()
        numSerie.text = batterie.numeroSerie
        subscriptions += storeSubject.map { it.batteries }
            .map { lb -> lb.filter { it.numeroSerie == batterie.numeroSerie } }
            .map { lb -> lb.map { it.dateDernierCycle } }
            .map { it.filterNotNull() }
            .map { it.max()?.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)) ?: "Jamais effectué" }
            .distinctUntilChanged()
            .subscribe { dateDernierCycle.text = it }

        modify.setOnMouseClicked {
            DialogUtil.showCreateDialog<GridPane, CreateBatterieDialog, Batterie>(
                util = DialogUtil(
                    component = Components.CREATE_BATTERIE_DIALOG,
                    dialogOwner = modify.scene.window,
                    dialogTitle = "Modification d'une batterie"
                ),
                afterCreationControllerInit = { it.init(batterie.drone, batterie) },
                resultConverter = { buttonType, controller -> if (buttonType == ButtonType.OK) controller.getData() else null },
                onResultPresent = { Olga.STORE.dispatch(Modify(batterie, it)) }
            )
        }

        updateDateDernierCycle.setOnMouseClicked {
            val telepilote = showTelepiloteDialog()
            if (telepilote != null) {
                val copy = batterie.copy()
                copy.dateDernierCycle = LocalDateTime.now()
                Olga.STORE.dispatch(Modify(batterie, copy))
                Olga.STORE.dispatch(Add(CarnetMaintenanceBatterie {
                    this.batterie = batterie
                    this.telepilote = telepilote
                    date = LocalDateTime.now()
                    notes = "Cycle de batterie"
                }))
            }
        }

        val lastGridLine = grid.children.takeLast(grid.columnConstraints.size)
        val lastGridConstraint = grid.rowConstraints[grid.rowConstraints.lastIndex]
        subscriptions += storeSubject.map { it.santes }
            .map { ls -> ls.filter { it.batterie.numeroSerie == batterie.numeroSerie } }
            .map { ls -> ls.maxBy { it.date }.toOption() }
            .distinctUntilChanged { o1, o2 -> o1.orNull() == o2.orNull() }
            .subscribe {
                if (it.isDefined()) {
                    val definedSante = it.orNull()!!
                    this.sante.style =
                        "-fx-background-color: #${definedSante.getSanteValueFromPourcentage().couleur.extractHexString()};"
                    if (grid.rowConstraints.size < 4) {
                        grid.children += lastGridLine
                        grid.rowConstraints += lastGridConstraint
                    }
                } else {
                    grid.children -= lastGridLine
                    grid.rowConstraints -= lastGridConstraint
                }
            }

        entretienButton.setOnMouseClicked { Olga.BUS.send(SwitchScene(Components.ENTRETIEN_BATTERIE, batterie)) }
    }

    private fun showTelepiloteDialog(): Telepilote? {
        val telepiloteDialog = TextInputDialog()
        telepiloteDialog.title = "Sélection du télépilote"
        telepiloteDialog.headerText =
            "Quel télépilote va effectuer cette maintenance ?\nCette information est obligatoire pour la réaliser."
        telepiloteDialog.contentText = "Nom du télépilote"

        telepiloteDialog.initOwner(grid.scene.window)

        TextFields.bindAutoCompletion(telepiloteDialog.editor) { param: AutoCompletionBinding.ISuggestionRequest? ->
            val userText = param?.userText ?: ""
            Olga.STORE.state.telepilotes
                .map { telepilote -> telepilote.getNomPrenom() }
                .filter { telepilote -> telepilote.contains(userText, ignoreCase = true) }
        }

        return telepiloteDialog.showAndWait()
            .map { t -> Olga.STORE.state.telepilotes.find { it.getNomPrenom() == t } }
            .orElse(null)
    }

}
