package org.pyerohdc.olga.view.drone

import javafx.beans.binding.Bindings
import javafx.beans.binding.BooleanExpression
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.DatePicker
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import org.controlsfx.control.textfield.AutoCompletionBinding
import org.controlsfx.control.textfield.TextFields
import org.fxmisc.easybind.EasyBind
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.Carnet
import org.pyerohdc.olga.view.DataDialog
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.util.toBooleanBinding
import java.net.URL
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

class CreateCarnetDialog : FXMLController, Initializable, DataDialog {

    @FXML
    lateinit var date: DatePicker

    @FXML
    lateinit var telepilote: TextField

    @FXML
    private lateinit var notes: TextArea

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        date.value = LocalDate.now()

        TextFields.bindAutoCompletion(telepilote) { param: AutoCompletionBinding.ISuggestionRequest? ->
            val userText = param?.userText ?: ""
            Olga.STORE.state.telepilotes
                .map { telepilote -> telepilote.getNomPrenom() }
                .filter { telepilote -> telepilote.contains(userText, ignoreCase = true) }
        }
    }

    fun getData() = object : Carnet {
        override var telepilote = findTelepilote()
        override var date = this@CreateCarnetDialog.date.value.atTime(LocalTime.now())
        override var notes = this@CreateCarnetDialog.notes.text.let { if (it.isBlank()) null else it }
    }

    override fun invalidProperty(): BooleanExpression = Bindings.or(
        date.valueProperty().isNull,
        // Invalide si la valeur du champ ne correspond à aucun télépilote (il faut passer par l'auto-complétion)
        EasyBind
            .map(telepilote.textProperty()) { t -> Olga.STORE.state.telepilotes.none { telepilote -> telepilote.getNomPrenom() == t } }
            .toBooleanBinding()
    )

    override fun initDialog() = date.requestFocus()

    private fun findTelepilote() = Olga.STORE.state.telepilotes.first { it.getNomPrenom() == telepilote.text }

}
