package org.pyerohdc.olga.view

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.layout.GridPane
import javafx.scene.paint.Color
import jiconfont.icons.font_awesome.FontAwesome
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.redux.Add
import org.pyerohdc.olga.view.util.DialogUtil
import org.pyerohdc.olga.view.util.getIconNode
import java.net.URL
import java.util.*

class CreateDroneOrTelepiloteButton : FXMLController, Initializable {

    @FXML
    private lateinit var create: Button

    private lateinit var type: Either<Class<Telepilote>, Class<Drone>>

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        create.graphic = getIconNode(
            iconCode = FontAwesome.PLUS,
            size = 32,
            fillColor = Color.GREEN
        )

        create.setOnMouseClicked { showCreateDialog() }
    }

    fun initCreator(type: Either<Class<Telepilote>, Class<Drone>>) {
        this.type = type

        create.text = "Ajouter un " + when (type) {
            is Left -> "télépilote"
            is Right -> "drone"
        }
    }

    private fun showCreateDialog() {
        DialogUtil.showCreateDialog<GridPane, CreateDroneOrTelepiloteDialog, Either<Telepilote, Drone>>(
            util = DialogUtil(
                component = Components.CREATE_DRONE_OR_TELEPILOTE_DIALOG,
                dialogOwner = create.scene.window,
                dialogTitle = "Création d'un " + if (type is Left) "télépilote" else "drone"
            ),
            afterCreationControllerInit = { it.initForm(type) },
            resultConverter = { buttonType, controller -> if (buttonType == ButtonType.OK) controller.getData(type) else null },
            onResultPresent = {
                val action = when (it) {
                    is Left -> Add(it.a)
                    is Right -> Add(it.b)
                }

                Olga.STORE.dispatch(action)
            }
        )
    }

}
