package org.pyerohdc.olga.view

import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.TableView
import jiconfont.icons.font_awesome.FontAwesome
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.HasActif
import org.pyerohdc.olga.entities.HasIdAndEntity
import org.pyerohdc.olga.redux.Modify
import org.pyerohdc.olga.view.util.getIconNode
import java.net.URL
import java.util.*

class RestoreEntitiesButtonBar : FXMLController, Initializable {

    @FXML
    private lateinit var selectAll: Button

    @FXML
    private lateinit var deselectAll: Button

    @FXML
    private lateinit var invertSelection: Button

    @FXML
    private lateinit var save: Button

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        val size = 21

        selectAll.graphic = getIconNode(
            iconCode = FontAwesome.CHECK_SQUARE_O,
            size = size
        )

        deselectAll.graphic = getIconNode(
            iconCode = FontAwesome.MINUS_SQUARE_O,
            size = size
        )

        invertSelection.graphic = getIconNode(
            iconCode = FontAwesome.DELICIOUS,
            size = size
        )

        save.graphic = getIconNode(
            iconCode = FontAwesome.FLOPPY_O,
            size = size
        )
    }

    fun <T, E> initHandlers(table: TableView<T>) where
            T : HasActifProperty,
            T : HasOriginalData<E>,
            E : HasIdAndEntity<E>,
            E : HasActif {
        selectAll.setOnMouseClicked { table.items.forEach { item -> item.actif.value = true } }

        deselectAll.setOnMouseClicked { table.items.forEach { item -> item.actif.value = false } }

        invertSelection.setOnMouseClicked { table.items.forEach { item -> item.actif.value = !item.actif.value } }

        save.setOnMouseClicked {
            table.items
                .filter { item -> item.actif.value }
                .forEach { item ->
                    Olga.STORE.dispatch(
                        Modify(
                            item.originalData,
                            item.originalData.copy().also { it.actif = item.actif.value })
                    )
                }
        }
    }

}
