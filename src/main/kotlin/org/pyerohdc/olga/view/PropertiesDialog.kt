package org.pyerohdc.olga.view

import io.reactivex.rxjavafx.observables.JavaFxObservable
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.CheckBox
import javafx.scene.control.Label
import javafx.scene.control.Spinner
import jiconfont.icons.font_awesome.FontAwesome
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.redux.ClearAllData
import org.pyerohdc.olga.redux.LoadAllData
import org.pyerohdc.olga.view.util.LongSpinnerValueFactory
import org.pyerohdc.olga.view.util.getIconNode
import java.net.URL
import java.util.*
import java.util.concurrent.TimeUnit

class PropertiesDialog : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var refresh: CheckBox

    @FXML
    private lateinit var refreshDelayLabel: Label

    @FXML
    private lateinit var refreshDelaySpinner: Spinner<Long>

    @FXML
    private lateinit var changeDatabaseLocationButton: Button

    @FXML
    private lateinit var path: Label

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        refresh.isSelected = Olga.APP_PROPERTIES.refresh
        subscriptions += JavaFxObservable.valuesOf(refresh.selectedProperty())
            .subscribe {
                Olga.APP_PROPERTIES.refresh = it

                refreshDelayLabel.isDisable = !it
                refreshDelaySpinner.isDisable = !it
            }

        refreshDelaySpinner.valueFactory = LongSpinnerValueFactory(1, Long.MAX_VALUE, Olga.APP_PROPERTIES.refreshDelay)
        refreshDelaySpinner.setOnScroll {
            if (it.deltaY > 0) refreshDelaySpinner.increment()
            else refreshDelaySpinner.decrement()
        }
        subscriptions += JavaFxObservable.valuesOf(refreshDelaySpinner.valueProperty())
            .debounce(1, TimeUnit.SECONDS)
            .subscribe {
                Olga.APP_PROPERTIES.refreshDelay = it
            }

        changeDatabaseLocationButton.graphic = getIconNode(
            iconCode = FontAwesome.DATABASE,
            size = changeDatabaseLocationButton.prefHeight * (5.0 / 8.0)
        )
        changeDatabaseLocationButton.setOnMouseClicked {
            OlgaApplication.selectDatabaseLocation(changeDatabaseLocationButton.scene.window, blockOnCancel = false)
            Olga.reloadDatabaseConfiguration()
            Olga.STORE.dispatch(ClearAllData)
            Olga.STORE.dispatch(LoadAllData)

            path.text = Olga.APP_PROPERTIES.databaseLocation
        }

        path.text = Olga.APP_PROPERTIES.databaseLocation
    }

}
