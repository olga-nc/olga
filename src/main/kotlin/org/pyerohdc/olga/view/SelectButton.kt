package org.pyerohdc.olga.view

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import arrow.core.left
import arrow.core.right
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.layout.GridPane
import javafx.scene.paint.Color
import jiconfont.icons.font_awesome.FontAwesome
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.bus.SwitchScene
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.redux.Modify
import org.pyerohdc.olga.view.util.DialogUtil
import org.pyerohdc.olga.view.util.getIconNode
import java.net.URL
import java.util.*

class SelectButton : FXMLController, Initializable {

    @FXML
    private lateinit var select: Button

    @FXML
    private lateinit var modify: Button

    @FXML
    private lateinit var delete: Button

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        modify.graphic = getIconNode(
            iconCode = FontAwesome.PENCIL,
            fillColor = Color.CORNFLOWERBLUE,
            size = modify.prefHeight / 2
        )
        modify.text = ""

        delete.graphic = getIconNode(
            iconCode = FontAwesome.MINUS,
            fillColor = Color.RED,
            size = delete.prefHeight / 2
        )
        delete.text = ""
    }

    fun initData(data: Either<Telepilote, Drone>) {
        select.text = when (data) {
            is Left -> data.a.getPrenomNom()
            is Right -> data.b.getNomMatricule()
        }

        select.setOnMouseClicked {
            when (data) {
                is Left -> Olga.BUS.send(SwitchScene(Components.ECRAN_TELEPILOTE, data.a))
                is Right -> Olga.BUS.send(SwitchScene(Components.ECRAN_DRONE, data.b))
            }
        }

        modify.setOnMouseClicked { showModifyDialog(data) }

        delete.setOnMouseClicked { showDeleteDialog(data) }
    }

    private fun showModifyDialog(data: Either<Telepilote, Drone>) {
        DialogUtil.showCreateDialog<GridPane, CreateDroneOrTelepiloteDialog, Either<Telepilote, Drone>>(
            util = DialogUtil(
                component = Components.CREATE_DRONE_OR_TELEPILOTE_DIALOG,
                dialogOwner = modify.scene.window,
                dialogTitle = "Modification d'un " + if (data is Left) "télépilote" else "drone"
            ),
            afterCreationControllerInit = { it.initData(data) },
            resultConverter = { buttonType, controller ->
                if (buttonType == ButtonType.OK) controller.getData(
                    when (data) {
                        is Left -> data.a.javaClass.left()
                        is Right -> data.b.javaClass.right()
                    }
                ) else null
            },
            onResultPresent = {
                val action = when (it) {
                    is Left -> Modify((data as Left).a, it.a)
                    is Right -> Modify((data as Right).b, it.b)
                }

                Olga.STORE.dispatch(action)
            }
        )
    }

    private fun showDeleteDialog(data: Either<Telepilote, Drone>) {
        val typeLabel = if (data is Left) "télépilote" else "drone"
        val dataLabel = when (data) {
            is Left -> data.a.getPrenomNom()
            is Right -> data.b.getNomMatricule()
        }

        val alert = Alert(Alert.AlertType.CONFIRMATION)
        alert.initOwner(select.scene.window)
        alert.title = "Désactivation d'un $typeLabel"
        alert.contentText = """
                    Êtes-vous sûr de vouloir désactiver le $typeLabel "$dataLabel" ?
                    Cela ne supprimera pas ses données associées.
                """.trimIndent()
        alert.showAndWait()
            .filter { it == ButtonType.OK }
            .ifPresent {
                Olga.STORE.dispatch(
                    when (data) {
                        is Left -> {
                            val copy: Telepilote = data.a.copy()
                            copy.actif = false
                            Modify(data.a, copy)
                        }
                        is Right -> {
                            val copy: Drone = data.b.copy()
                            copy.actif = false
                            Modify(data.b, copy)
                        }
                    }
                )
            }
    }

}
