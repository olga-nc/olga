package org.pyerohdc.olga.view

import arrow.core.left
import arrow.core.right
import io.reactivex.rxjavafx.observables.JavaFxObservable
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import javafx.scene.layout.BorderPane
import javafx.scene.layout.GridPane
import javafx.scene.layout.VBox
import javafx.stage.StageStyle
import jiconfont.icons.font_awesome.FontAwesome
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.bus.SwitchScene
import org.pyerohdc.olga.view.util.DialogUtil
import org.pyerohdc.olga.view.util.FXMLLoaderUtil
import org.pyerohdc.olga.view.util.getIconNode
import java.net.URL
import java.util.*

class MainView : FXMLController, WithSubscriptions(), Initializable {

    @FXML
    private lateinit var restoreDeactivatedEntities: Button

    @FXML
    private lateinit var displayProperties: Button

    @FXML
    private lateinit var telepiloteContainer: BorderPane

    private lateinit var telepiloteController: SelectDomain

    @FXML
    private lateinit var droneContainer: BorderPane

    private lateinit var droneController: SelectDomain

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        initStoreSubscription()

        // La taille de restoreDeactivatedEntities est définie dynamiquement par JavaFX. On attend donc qu'elle soit définie, et on génère l'icône après ça.
        // L'observable s'auto-désabonne avec le take(1)
        JavaFxObservable.valuesOf(restoreDeactivatedEntities.heightProperty())
            .map { it.toDouble() }
            .filter { it > 0 }
            .take(1)
            .subscribe {
                restoreDeactivatedEntities.graphic = getIconNode(
                    iconCode = FontAwesome.UNDO,
                    size = it * (5.0 / 8.0)
                )
            }
        restoreDeactivatedEntities.setOnMouseClicked {
            Olga.BUS.send(SwitchScene(Components.RESTORE_ENTITIES_VIEW))
        }

        JavaFxObservable.valuesOf(displayProperties.heightProperty())
            .map { it.toDouble() }
            .filter { it > 0 }
            .take(1)
            .subscribe {
                displayProperties.graphic = getIconNode(
                    iconCode = FontAwesome.SLIDERS,
                    size = it * (5.0 / 8.0)
                )
            }
        displayProperties.setOnMouseClicked {
            val dialogComponent = FXMLLoaderUtil.load<GridPane, PropertiesDialog>(Components.PROPERTIES_DIALOG)

            val dialog = Dialog<Nothing>()
            val dialogPane = dialog.dialogPane
            dialog.title = "Propriétés"
            dialog.isResizable = true
            dialogPane.buttonTypes += ButtonType.CLOSE
            dialogPane.content = dialogComponent.node

            dialog.initStyle(StageStyle.UTILITY)
            dialog.initOwner(displayProperties.scene.window)
            dialog.setOnCloseRequest { Olga.APP_PROPERTIES.writeProps(Olga.getJarFolderPath().toFile()) }

            DialogUtil.setDialogMinSize(dialog)

            dialog.showAndWait()
        }

        subscriptions += storeSubject
            .map { it.telepilotes.filter { telepilote -> telepilote.actif } }
            .distinctUntilChanged()
            .subscribe {
                val selectTelepiloteZone = FXMLLoaderUtil.load<VBox, SelectDomain>(Components.SELECT_DOMAIN)
                telepiloteController = selectTelepiloteZone.controller
                telepiloteController.initData(it.left())
                telepiloteContainer.center = selectTelepiloteZone.node
            }

        subscriptions += storeSubject
            .map { it.drones.filter { drone -> drone.actif } }
            .distinctUntilChanged()
            .subscribe {
                val selectDroneZone = FXMLLoaderUtil.load<VBox, SelectDomain>(Components.SELECT_DOMAIN)
                droneController = selectDroneZone.controller
                droneController.initData(it.right())
                droneContainer.center = selectDroneZone.node
            }
    }

}
