package org.pyerohdc.olga.view

import javafx.beans.property.BooleanProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.beans.value.ObservableValue
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.cell.CheckBoxTableCell
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import org.pyerohdc.olga.AppState
import org.pyerohdc.olga.bus.SwitchScene
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.HasActif
import org.pyerohdc.olga.entities.HasIdAndEntity
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.entities.Vol
import org.pyerohdc.olga.view.util.Column
import org.pyerohdc.olga.view.util.FXMLLoaderUtil
import org.pyerohdc.olga.view.util.RetourUtil
import org.pyerohdc.olga.view.util.SceneProperties
import org.pyerohdc.olga.view.util.resizeColumnsToFitContent
import java.net.URL
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.jvm.javaGetter
import kotlin.reflect.typeOf

@SceneProperties(prefWidth = 720.0)
class RestoreEntitiesView : FXMLController, Initializable, WithSubscriptions() {

    @FXML
    private lateinit var retour: Button

    @FXML
    private lateinit var telepiloteContainer: BorderPane

    @FXML
    private lateinit var droneContainer: BorderPane

    @FXML
    private lateinit var batterieContainer: BorderPane

    @ExperimentalStdlibApi
    override fun initialize(location: URL?, resources: ResourceBundle?) {
        RetourUtil.initialize(retour) { SwitchScene(Components.MAIN_VIEW) }

        initStoreSubscription()

        populateTelepiloteTable()
        populateDroneTable()
        populateBatterieTable()
    }

    @ExperimentalStdlibApi
    private fun populateTelepiloteTable() {
        @Suppress("unused")
        populateTable(TelepilotePropertified::class, telepiloteContainer,
            {
                it.telepilotes.filter { t -> !t.actif }
                    .map { t -> t to it.vols.filter { v -> v.telepilote.id == t.id } }
                    .sortedBy { t -> t.first.getPrenomNom() }
            },
            { TelepilotePropertified(it.first, it.second) })
    }

    @ExperimentalStdlibApi
    private fun populateDroneTable() {
        populateTable(DronePropertified::class, droneContainer,
            {
                it.drones.filter { t -> !t.actif }
                    .map { d -> d to it.batteries.filter { b -> b.drone.id == d.id } }
                    .sortedBy { d -> d.first.getNomMatricule() }
            },
            { DronePropertified(it.first, it.second) })
    }

    @ExperimentalStdlibApi
    private fun populateBatterieTable() {
        populateTable(
            BatteriePropertified::class,
            batterieContainer,
            {
                it.batteries
                    .filter { b -> !b.actif }
                    .sortedBy { b -> b.numeroSerie }
            },
            { BatteriePropertified(it) })
    }

    @ExperimentalStdlibApi
    private fun <T, E, D> populateTable(
        tableType: KClass<T>,
        container: BorderPane,
        stateMapper: (AppState) -> List<D>,
        dataPropertifier: (D) -> T
    ) where
            T : HasActifProperty,
            T : HasOriginalData<E>,
            E : HasIdAndEntity<E>,
            E : HasActif {
        val table = createTable(tableType)

        val restoreEntitiesButtonBar =
            FXMLLoaderUtil.load<HBox, RestoreEntitiesButtonBar>(Components.RESTORE_ENTITIES_BUTTON_BAR)
                .also { it.controller.initHandlers(table) }

        container.top = restoreEntitiesButtonBar.node
        container.center = table
        subscriptions += storeSubject
            .map { stateMapper(it) }
            .distinctUntilChanged()
            .map { it.map { d -> dataPropertifier(d) } }
            .subscribe { table.items.setAll(it) }
    }

    @ExperimentalStdlibApi
    private fun <T : HasActifProperty> createTable(type: KClass<T>): TableView<T> {

        @Suppress("UNUSED_PARAMETER", "UNCHECKED_CAST")
        fun <S : Any> createTableColumn(columnType: KClass<S>, property: KProperty1<T, *>): TableColumn<T, S> {
            return TableColumn<T, S>()
                .also { it.setCellValueFactory { param -> property.javaGetter!!(param.value) as ObservableValue<S> } }
                .also { if (columnType == Boolean::class) it.setCellFactory { CheckBoxTableCell() } }
        }

        val table = TableView<T>()
        table.columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY
        table.isEditable = true

        table.columns += type.declaredMemberProperties.asSequence()
            .filter { it.annotations.any { a -> a is Column } }
            .map { it to it.annotations.first { a -> a is Column } as Column }
            .sortedBy { it.second.order }
            .map {
                val property = it.first
                val columnAnnotation = it.second

                val columnType = when (property.returnType) {
                    typeOf<StringProperty>() -> String::class
                    typeOf<BooleanProperty>() -> Boolean::class
                    typeOf<ObjectProperty<Int>>() -> Int::class
                    else -> error("Je sais pas gérer ça")
                }

                val tableColumn = createTableColumn(columnType, property)
                tableColumn.text = columnAnnotation.label

                tableColumn.isEditable = columnType == Boolean::class

                tableColumn
            }
            .toList()

        table.resizeColumnsToFitContent()

        return table
    }

    @Suppress("unused")
    private class TelepilotePropertified(
        @Column("Actif", 0) override val actif: BooleanProperty,
        @Column("Nom", 1) val prenomNom: StringProperty,
        @Column("Nombre de vols", 2) val vols: ObjectProperty<Int>,
        override val originalData: Telepilote
    ) : HasActifProperty, HasOriginalData<Telepilote> {
        constructor(telepilote: Telepilote, vols: List<Vol>) : this(
            SimpleBooleanProperty(telepilote.actif),
            SimpleStringProperty(telepilote.getPrenomNom()),
            SimpleObjectProperty(vols.size),
            telepilote
        )
    }

    @Suppress("unused")
    private class DronePropertified(
        @Column("Actif", 0) override val actif: BooleanProperty,
        @Column("Dénomination", 1) val nomMatricule: StringProperty,
        @Column("Nombre de batteries", 2) val batteries: ObjectProperty<Int>,
        override val originalData: Drone
    ) : HasActifProperty, HasOriginalData<Drone> {
        constructor(drone: Drone, batteries: List<Batterie>) : this(
            SimpleBooleanProperty(drone.actif),
            SimpleStringProperty(drone.getNomMatricule()),
            SimpleObjectProperty(batteries.size),
            drone
        )
    }

    @Suppress("unused")
    private class BatteriePropertified(
        @Column("Actif", 0) override val actif: BooleanProperty,
        @Column("Numéro de série", 1) val numeroSerie: StringProperty,
        @Column("Drone", 2) val drone: StringProperty,
        override val originalData: Batterie
    ) : HasActifProperty, HasOriginalData<Batterie> {
        constructor(batterie: Batterie) : this(
            SimpleBooleanProperty(batterie.actif),
            SimpleStringProperty(batterie.numeroSerie),
            SimpleStringProperty(batterie.drone.getNomMatricule()),
            batterie
        )
    }

}
