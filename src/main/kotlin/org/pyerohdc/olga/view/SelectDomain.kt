package org.pyerohdc.olga.view

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.HBox
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.view.util.FXMLLoaderUtil

class SelectDomain : FXMLController {

    @FXML
    private lateinit var domainLabel: Label

    @FXML
    private lateinit var buttonsUpperBox: HBox

    @FXML
    private lateinit var buttonsScrollPane: ScrollPane

    @FXML
    private lateinit var buttonsBox: HBox

    fun initData(data: Either<List<Telepilote>, List<Drone>>) {
        domainLabel.text = when (data) {
            is Left -> "Télépilotes"
            is Right -> "Drones"
        }

        buttonsBox.children += when (data) {
            is Left -> data.a.map { getSelectButtonNode(it) }
            is Right -> data.b.map { getSelectButtonNode(it) }
        }
        buttonsScrollPane
            .setOnScroll {
                if (it.deltaX == 0.0 && it.deltaY != 0.0) buttonsScrollPane.hvalue =
                    buttonsScrollPane.hvalue - it.deltaY / buttonsBox.width
            }

        if (buttonsBox.children.isEmpty()) {
            buttonsUpperBox.children.clear()
        }
        buttonsUpperBox.children += getCreateButton(data)
    }

    private fun getSelectButtonNode(telepilote: Telepilote) = getSelectButtonNode(Left(telepilote))

    private fun getSelectButtonNode(drone: Drone) = getSelectButtonNode(Right(drone))

    private fun getSelectButtonNode(data: Either<Telepilote, Drone>): AnchorPane {
        val selectButton = FXMLLoaderUtil.load<AnchorPane, SelectButton>(Components.SELECT_BUTTON)
        selectButton.controller.initData(data)
        return selectButton.node
    }

    private fun getCreateButton(data: Either<List<Telepilote>, List<Drone>>): AnchorPane {
        val type: Either<Class<Telepilote>, Class<Drone>> = when (data) {
            is Left -> Left(Telepilote::class.java)
            is Right -> Right(Drone::class.java)
        }

        val createButton =
            FXMLLoaderUtil.load<AnchorPane, CreateDroneOrTelepiloteButton>(Components.CREATE_DRONE_OR_TELEPILOTE_BUTTON)
        createButton.controller.initCreator(type)

        return createButton.node
    }

}
