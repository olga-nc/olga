package org.pyerohdc.olga.view.util

import javafx.beans.NamedArg
import javafx.beans.property.LongProperty
import javafx.beans.property.SimpleLongProperty
import javafx.scene.control.SpinnerValueFactory
import javafx.util.converter.LongStringConverter

class LongSpinnerValueFactory constructor(
    @NamedArg("min") min: Long,
    @NamedArg("max") max: Long,
    @NamedArg("initialValue") initialValue: Long = min,
    @NamedArg("amountToStepBy") amountToStepBy: Long = 1
) :
    SpinnerValueFactory<Long>() {

    private val minProperty: LongProperty = object : SimpleLongProperty(this, "minProperty") {
        override fun invalidated() {
            val classInstance = this@LongSpinnerValueFactory
            val currentValue = classInstance.value ?: return
            val newMin = get()
            if (newMin > classInstance.max) {
                classInstance.min = classInstance.max
                return
            }
            if (currentValue < newMin) {
                classInstance.value = newMin
            }
        }
    }

    var min
        get() = minProperty.get()
        set(value) = minProperty.set(value)

    fun minProperty(): LongProperty {
        return minProperty
    }

    private val maxProperty: LongProperty = object : SimpleLongProperty(this, "maxProperty") {
        override fun invalidated() {
            val classInstance = this@LongSpinnerValueFactory
            val currentValue = classInstance.value ?: return
            val newMax = get()
            if (newMax < classInstance.min) {
                classInstance.max = (classInstance.min)
                return
            }
            if (currentValue > newMax) {
                classInstance.value = newMax
            }
        }
    }

    var max
        get() = maxProperty.get()
        set(value) = maxProperty.set(value)

    fun maxProperty(): LongProperty {
        return maxProperty
    }

    private val amountToStepByProperty: LongProperty = SimpleLongProperty(this, "amountToStepByProperty")

    var amountToStepBy: Long
        get() = amountToStepByProperty.get()
        set(value) = amountToStepByProperty.set(value)

    fun amountToStepByProperty(): LongProperty {
        return amountToStepByProperty
    }

    override fun decrement(steps: Int) {
        val min = min
        val max = max
        val newIndex = value!! - steps * amountToStepBy
        value = when {
            newIndex >= min -> newIndex
            isWrapAround -> wrapValue(
                newIndex,
                min,
                max
            ) + 1
            else -> min
        }
    }

    override fun increment(steps: Int) {
        val min = min
        val max = max
        val currentValue = value!!
        val newIndex = currentValue + steps * amountToStepBy
        value = when {
            newIndex <= max -> newIndex
            isWrapAround -> wrapValue(
                newIndex,
                min,
                max
            ) - 1
            else -> max
        }
    }

    init {
        this.min = min
        this.max = max
        this.amountToStepBy = amountToStepBy
        converter = LongStringConverter()
        valueProperty().addListener { _, _, _ ->
            // when the value is set, we need to react to ensure it is a
            // valid value (and if not, blow up appropriately)
            value = value.coerceIn(this.min..this.max)
        }
        value = value?.coerceIn(this.min..this.max) ?: this.min
    }

    private fun wrapValue(value: Long, min: Long, max: Long): Long {
        val mod = value % (max - min)
        return (min until max).asSequence().filterIndexed { index, _ -> mod == index.toLong() }.first()
    }

}
