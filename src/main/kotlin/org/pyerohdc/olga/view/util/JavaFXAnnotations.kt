package org.pyerohdc.olga.view.util

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY)
annotation class Column(
    val label: String,
    val order: Int
)

@Target(AnnotationTarget.CLASS)
annotation class SceneProperties(
    val prefWidth: Double = -1.0
)
