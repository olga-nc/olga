package org.pyerohdc.olga.view.util

import com.sun.javafx.scene.control.skin.TableViewSkin
import io.reactivex.rxjavafx.observables.JavaFxObservable
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler
import javafx.scene.control.TableView
import java.util.concurrent.TimeUnit
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.jvm.isAccessible

fun TableView<*>.resizeColumnsToFitContent() {
    val resizeFunction = TableViewSkin::class.declaredFunctions.find { it.name == "resizeColumnToFitContent" }

    if (resizeFunction != null) {
        resizeFunction.isAccessible = true

        // On ne prend que le premier changement de valeur, on attend 150 millisecondes, et sur le thread JavaFX, on appelle la méthode
        JavaFxObservable.valuesOf(this.skinProperty())
            .take(1)
            .delay(150, TimeUnit.MILLISECONDS)
            .observeOn(JavaFxScheduler.platform())
            .subscribe { this.columns.forEach { column -> resizeFunction.call(this.skin, column, -1) } }
    }
}
