package org.pyerohdc.olga.view.util

import javafx.scene.control.Button
import jiconfont.icons.font_awesome.FontAwesome
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.bus.SwitchScene

class RetourUtil {

    companion object {
        @JvmStatic
        fun initialize(retourButton: Button, switchSceneSupplier: () -> SwitchScene) {
            retourButton.text = ""
            retourButton.graphic = getIconNode(
                iconCode = FontAwesome.ARROW_LEFT,
                size = retourButton.prefHeight / 2
            )
            retourButton.setOnMouseClicked { Olga.BUS.send(switchSceneSupplier()) }
        }
    }

}
