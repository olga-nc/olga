package org.pyerohdc.olga.view.util

import javafx.beans.binding.Bindings
import javafx.beans.binding.BooleanBinding
import org.fxmisc.easybind.monadic.MonadicBinding

fun MonadicBinding<Boolean>.toBooleanBinding(): BooleanBinding? = Bindings.selectBoolean(this)
