package org.pyerohdc.olga.view.util

import javafx.beans.binding.Bindings
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.entities.HasIdAndEntity
import org.pyerohdc.olga.redux.Delete

fun <S, T : HasIdAndEntity<T>> injectContextMenu(
    table: TableView<S>,
    row: TableRow<S>,
    title: String,
    contentText: String,
    dataToDelete: T
) {
    val deleteMenuItem = MenuItem("Supprimer")
    deleteMenuItem.setOnAction {
        val alert = Alert(Alert.AlertType.CONFIRMATION)
        alert.initOwner(table.scene.window)
        alert.title = title
        alert.contentText = contentText

        alert.showAndWait()
            .filter { b -> b == ButtonType.OK }
            .ifPresent { Olga.STORE.dispatch(Delete(dataToDelete)) }
    }
    val contextMenu = ContextMenu(deleteMenuItem)

    row.contextMenuProperty().bind(
        Bindings.`when`(row.emptyProperty().not())
            .then(contextMenu)
            .otherwise(null as ContextMenu?)
    )
}
