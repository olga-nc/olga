package org.pyerohdc.olga.view.util

import io.reactivex.rxjavafx.observables.JavaFxObservable
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler
import javafx.application.Platform
import javafx.scene.Node
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.stage.Window
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.DataDialog
import org.pyerohdc.olga.view.FXMLController
import java.util.concurrent.TimeUnit

class DialogUtil(
    val component: Components,
    val dialogOwner: Window,
    val dialogTitle: String = ""
) {

    companion object {
        fun <N : Node, C, T> showCreateDialog(
            util: DialogUtil,
            afterCreationControllerInit: (C) -> Unit = {},
            @Suppress("UNUSED_ANONYMOUS_PARAMETER") resultConverter: (ButtonType, C) -> T? = { buttonType, controller -> null },
            onResultPresent: (T) -> Unit = {}
        )
                where C : FXMLController,
                      C : DataDialog {
            val createDialog =
                FXMLLoaderUtil.load<N, C>(util.component).also { afterCreationControllerInit(it.controller) }
            val createDialogController = createDialog.controller

            val dialog = Dialog<T>()
            val dialogPane = dialog.dialogPane
            dialog.title = util.dialogTitle
            dialog.isResizable = true
            dialogPane.buttonTypes.addAll(ButtonType.OK, ButtonType.CANCEL)
            dialogPane.content = createDialog.node

            val okButton = dialogPane.lookupButton(ButtonType.OK)
            okButton.disableProperty().bind(createDialogController.invalidProperty())

            dialog.setResultConverter { resultConverter(it, createDialogController) }

            dialog.initStyle(StageStyle.UTILITY)
            dialog.initOwner(util.dialogOwner)

            setDialogMinSize(dialog)

            Platform.runLater { createDialogController.initDialog() }

            dialog.showAndWait()
                .ifPresent { onResultPresent(it) }
        }

        fun <T> setDialogMinSize(dialog: Dialog<T>) {
            // On est obligé d'envoyer la taille minimum une fois que la taille du dialog s'est
            // stabilisé, parce que la taille du dialogPane à l'initialisation est de 0
            val dialogPane = dialog.dialogPane
            JavaFxObservable.changesOf(dialogPane.heightProperty())
                .debounce(1, TimeUnit.SECONDS)
                .observeOn(JavaFxScheduler.platform())
                .take(1)
                .subscribe {
                    val stage = dialogPane.scene?.window as? Stage
                    stage?.minWidth = dialog.width
                    stage?.minHeight = dialog.height
                }
        }
    }

}
