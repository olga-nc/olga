package org.pyerohdc.olga.view.util

import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import jiconfont.IconCode
import jiconfont.javafx.IconNode

fun getIconNode(
    iconCode: IconCode,
    fillColor: Paint = Color.BLACK,
    size: Number = -1.0,
    stroke: Paint = Color.TRANSPARENT
): IconNode {
    val iconNode = IconNode(iconCode)
    iconNode.fill = fillColor
    iconNode.iconSize = size
    iconNode.stroke = stroke

    return iconNode
}
