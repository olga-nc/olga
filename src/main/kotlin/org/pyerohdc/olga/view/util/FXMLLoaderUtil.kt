package org.pyerohdc.olga.view.util

import javafx.fxml.FXMLLoader
import javafx.scene.Node
import org.pyerohdc.olga.Olga
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.FXMLController

class FXMLLoaderUtil {

    companion object {
        fun <N : Node, C : FXMLController> load(component: Components): Component<N, C> {
            val loader = FXMLLoader()

            loader.location = Olga::class.java.classLoader.getResource(component.getFile())

            val node = try {
                loader.load<N>()
            } catch (e: Exception) {
                throw RuntimeException(e)
            }

            val controller = loader.getController<C>()

            return Component(node, controller)
        }
    }

}

class Component<out N : Node, out C : FXMLController>(val node: N, val controller: C)
