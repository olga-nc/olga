package org.pyerohdc.olga.view.util

import javafx.beans.value.ObservableValue
import javafx.scene.control.TextInputControl

private const val pristineKey = "pristine"
private const val pristineListenerKey = "pristine"

/**
 * Ajoute la gestion du statut "pristine" au champ
 */
fun TextInputControl.handlePristineStatus() {
    this.properties.putIfAbsent(pristineKey, true)
    val listener: (ObservableValue<out String>, String, String) -> Unit =
        { _, _, _ ->
            this.properties[pristineKey] = false
            this.properties.remove(pristineListenerKey)
        }
    this.properties.putIfAbsent(pristineListenerKey, listener)
    this.textProperty().addListener(listener)
}

/**
 * Indique si le champ a déjà été modifié
 */
fun TextInputControl.isPristine(): Boolean {
    return this.properties.getOrDefault(pristineKey, false) as Boolean
}
