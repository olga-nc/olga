package org.pyerohdc.olga.view.util

import javafx.scene.control.Accordion

fun Accordion.avoidCompleteFold() {
    this.expandedPaneProperty().addListener { _, oldValue, _ ->
        // On "consomme" l'évènement si on essaye de replier un panneau
        val collapse = !this.panes.map { it.isExpanded }.fold(false) { a, b -> a || b }
        if (collapse && oldValue != null) {
            this.expandedPane = oldValue
        }
    }
}
