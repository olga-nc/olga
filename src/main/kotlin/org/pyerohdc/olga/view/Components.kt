package org.pyerohdc.olga.view

enum class Components(private val filePath: String) {

    MAIN_VIEW("MainView"),
    RESTORE_ENTITIES_VIEW("RestoreEntitiesView"),
    RESTORE_ENTITIES_BUTTON_BAR("RestoreEntitiesButtonBar"),
    PROPERTIES_DIALOG("PropertiesDialog"),
    SELECT_BUTTON("SelectButton"),
    CREATE_DRONE_OR_TELEPILOTE_BUTTON("CreateDroneOrTelepiloteButton"),
    CREATE_DRONE_OR_TELEPILOTE_DIALOG("CreateDroneOrTelepiloteDialog"),
    SELECT_DOMAIN("SelectDomain"),
    ECRAN_DRONE("drone/EcranDrone"),
    SYNTHESE_DRONE("drone/SyntheseDrone"),
    CARNET_VIEW("drone/CarnetView"),
    CREATE_CARNET_DIALOG("drone/CreateCarnetDialog"),
    SYNTHESE_BATTERIE("drone/SyntheseBatterie"),
    CREATE_BATTERIE_DIALOG("drone/CreateBatterieDialog"),
    ENTRETIEN_BATTERIE("drone/EntretienBatterie"),
    SANTE_VIEW("drone/SanteView"),
    CREATE_SANTE_DIALOG("drone/CreateSanteDialog"),
    ECRAN_TELEPILOTE("telepilote/EcranTelepilote"),
    SYNTHESE_TELEPILOTE("telepilote/SyntheseTelepilote"),
    VOL_VIEW("telepilote/VolView"),
    CREATE_VOL_DIALOG("telepilote/CreateVolDialog");

    companion object {
        @JvmStatic
        private val SCREENS_BASE_PATH = "${Components::class.java.`package`.name.replace('.', '/')}/"
    }

    fun getFile() = "$SCREENS_BASE_PATH$filePath.fxml"

}
