package org.pyerohdc.olga

import me.liuwj.ktorm.entity.add
import me.liuwj.ktorm.entity.findAll
import me.liuwj.ktorm.schema.Table
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.Batteries
import org.pyerohdc.olga.entities.CarnetChargeBatterie
import org.pyerohdc.olga.entities.CarnetChargeBatteries
import org.pyerohdc.olga.entities.CarnetMaintenanceBatterie
import org.pyerohdc.olga.entities.CarnetMaintenanceBatteries
import org.pyerohdc.olga.entities.CarnetMaintenancePiece
import org.pyerohdc.olga.entities.CarnetMaintenancePieces
import org.pyerohdc.olga.entities.Documents
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.Drones
import org.pyerohdc.olga.entities.HasId
import org.pyerohdc.olga.entities.HasIdAndEntity
import org.pyerohdc.olga.entities.Sante
import org.pyerohdc.olga.entities.Santes
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.entities.Telepilotes
import org.pyerohdc.olga.entities.Vol
import org.pyerohdc.olga.entities.Vols
import org.pyerohdc.olga.redux.Action
import org.pyerohdc.olga.redux.Add
import org.pyerohdc.olga.redux.ClearAllData
import org.pyerohdc.olga.redux.DbAction
import org.pyerohdc.olga.redux.Delete
import org.pyerohdc.olga.redux.LoadAllData
import org.pyerohdc.olga.redux.Modify
import org.pyerohdc.olga.redux.ReloadData

@Suppress("UNCHECKED_CAST")
fun initReducers() = { state: AppState, action: Action ->
    when (action) {
        is LoadAllData -> newFullState()
        is ClearAllData -> AppState()
        is ReloadData -> mergeStates(state, newFullState())
        is DbAction<*> -> when (action.type) {
            is Vol -> initReducer(
                action as DbAction<Vol>,
                state,
                Vols
            ) { appState, listTransformer -> appState.copy(vols = listTransformer(appState.vols)) }
            is Drone -> initReducer(
                action as DbAction<Drone>,
                state,
                Drones
            ) { appState, listTransformer -> appState.copy(drones = listTransformer(appState.drones)) }
            is Telepilote -> initReducer(
                action as DbAction<Telepilote>,
                state,
                Telepilotes
            ) { appState, listTransformer -> appState.copy(telepilotes = listTransformer(appState.telepilotes)) }
            is Batterie -> initReducer(
                action as DbAction<Batterie>,
                state,
                Batteries
            ) { appState, listTransformer -> appState.copy(batteries = listTransformer(appState.batteries)) }
            is CarnetMaintenancePiece -> initReducer(
                action as DbAction<CarnetMaintenancePiece>,
                state,
                CarnetMaintenancePieces
            ) { appState, listTransformer -> appState.copy(carnetsMaintenancePiece = listTransformer(appState.carnetsMaintenancePiece)) }
            is Sante -> initReducer(
                action as DbAction<Sante>,
                state,
                Santes
            ) { appState, listTransformer -> appState.copy(santes = listTransformer(appState.santes)) }
            is CarnetMaintenanceBatterie -> initReducer(
                action as DbAction<CarnetMaintenanceBatterie>,
                state,
                CarnetMaintenanceBatteries
            ) { appState, listTransformer -> appState.copy(carnetsMaintenanceBatterie = listTransformer(appState.carnetsMaintenanceBatterie)) }
            is CarnetChargeBatterie -> initReducer(
                action as DbAction<CarnetChargeBatterie>,
                state,
                CarnetChargeBatteries
            ) { appState, listTransformer -> appState.copy(carnetsChargeBatterie = listTransformer(appState.carnetsChargeBatterie)) }
            else -> throw NotImplementedError("Not yet implemented : ${action.type}")
        }
    }
}

private fun mergeStates(old: AppState, new: AppState): AppState {

    fun <T : HasId> mergeStatePart(old: List<T>, new: List<T>): List<T> {
        return new
            .asSequence()
            .plus(old)
            .distinctBy { it.id }
            .toList()
    }

    return AppState(
        mergeStatePart(old.telepilotes, new.telepilotes),
        mergeStatePart(old.drones, new.drones),
        mergeStatePart(old.batteries, new.batteries),
        mergeStatePart(old.carnetsChargeBatterie, new.carnetsChargeBatterie),
        mergeStatePart(old.carnetsMaintenanceBatterie, new.carnetsMaintenanceBatterie),
        mergeStatePart(old.carnetsMaintenancePiece, new.carnetsMaintenancePiece),
        mergeStatePart(old.santes, new.santes),
        mergeStatePart(old.vols, new.vols),
        mergeStatePart(old.documents, new.documents)
    )
}

/**
 * Initialise un reducer pour l'entité
 * @param dbAction l'action à effectuer (voir [DbAction])
 * @param state l'état d'origine du store
 * @param tableManager le gestionnaire d'entité
 * @param stateCopier l'opération qui permet de mettre à jour l'état du store pour une portion du store seulement
 * @return le nouvel état du store
 */
private fun <T : HasIdAndEntity<T>> initReducer(
    dbAction: DbAction<T>,
    state: AppState,
    tableManager: Table<T>,
    stateCopier: (AppState, (List<T>) -> List<T>) -> AppState
): AppState {
    return when (dbAction) {
        is Add<T> -> {
            tableManager.add(dbAction.value)
            stateCopier(state) { (it + dbAction.value).sortedBy { t -> t.id } }
        }
        is Delete<T> -> {
            dbAction.value.delete()
            stateCopier(state) { (it - dbAction.value).sortedBy { t -> t.id } }
        }
        is Modify<T> -> {
            val newValue = dbAction.newValue
            newValue.flushChanges()
            stateCopier(state) { list -> list.map { if (it.id == newValue.id) newValue else it }.sortedBy { it.id } }
        }
    }
}

private fun newFullState(): AppState {
    return AppState(
        Telepilotes.findAll(),
        Drones.findAll(),
        Batteries.findAll(),
        CarnetChargeBatteries.findAll(),
        CarnetMaintenanceBatteries.findAll(),
        CarnetMaintenancePieces.findAll(),
        Santes.findAll(),
        Vols.findAll(),
        Documents.findAll()
    )
}
