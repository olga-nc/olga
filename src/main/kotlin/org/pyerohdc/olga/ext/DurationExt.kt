package org.pyerohdc.olga.ext

import java.time.Duration

fun Duration.format() = "%02d:%02d".format(this.toHours(), this.toMinutes() % 60)
