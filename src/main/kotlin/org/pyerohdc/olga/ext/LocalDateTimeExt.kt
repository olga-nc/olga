package org.pyerohdc.olga.ext

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun LocalDateTime.formatLocalizedFullDateTime() = this.format(DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm:ss"))!!
