package org.pyerohdc.olga.ext

import javafx.scene.paint.Color

fun Color.extractHexString() = toString().drop(2).dropLast(2)
