package org.pyerohdc.olga

import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.CarnetChargeBatterie
import org.pyerohdc.olga.entities.CarnetMaintenanceBatterie
import org.pyerohdc.olga.entities.CarnetMaintenancePiece
import org.pyerohdc.olga.entities.Document
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.Sante
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.entities.Vol

data class AppState(
    val telepilotes: List<Telepilote>,
    val drones: List<Drone>,
    val batteries: List<Batterie>,
    val carnetsChargeBatterie: List<CarnetChargeBatterie>,
    val carnetsMaintenanceBatterie: List<CarnetMaintenanceBatterie>,
    val carnetsMaintenancePiece: List<CarnetMaintenancePiece>,
    val santes: List<Sante>,
    val vols: List<Vol>,
    val documents: List<Document>
) {
    constructor() : this(
        emptyList(),
        emptyList(),
        emptyList(),
        emptyList(),
        emptyList(),
        emptyList(),
        emptyList(),
        emptyList(),
        emptyList()
    )
}
