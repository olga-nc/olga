package org.pyerohdc.olga

import me.liuwj.ktorm.database.Database
import org.flywaydb.core.Flyway
import org.pyerohdc.olga.log.KtormMinLogger
import java.io.Closeable
import java.sql.Connection
import java.sql.DriverManager

class DatabaseConfiguration(databaseLocation: String) : Closeable {
    companion object {
        private const val USER = "olga"
        private const val PASSWORD = "olga1234"
    }

    private val databaseUrl = "jdbc:h2:$databaseLocation;AUTO_SERVER=TRUE;DB_CLOSE_DELAY=1"

    private val connection: Connection

    init {
        val flyway = Flyway.configure()
            .locations("org.pyerohdc.olga.migration")
            .dataSource(
                databaseUrl,
                USER,
                PASSWORD
            )
            .load()
        flyway.migrate()

        connection = DriverManager.getConnection(
            databaseUrl,
            USER,
            PASSWORD
        )

        Database.connect(logger = KtormMinLogger()) {
            object : Connection by connection {
                override fun close() {
                    // Override the close function and do nothing, keep the connection open.
                }
            }
        }
    }

    override fun close() {
        connection.close()
    }

}
