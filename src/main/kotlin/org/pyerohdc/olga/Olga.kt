package org.pyerohdc.olga

import com.esotericsoftware.minlog.Log
import javafx.application.Application
import javafx.scene.control.Spinner
import org.controlsfx.tools.ValueExtractor
import org.flywaydb.core.api.logging.LogFactory
import org.pyerohdc.olga.bus.RxBus
import org.pyerohdc.olga.log.ConsoleLogger
import org.pyerohdc.olga.log.FileLogger
import org.pyerohdc.olga.log.FlywayLogger
import org.pyerohdc.olga.redux.Action
import org.pyerohdc.olga.redux.Store
import org.pyerohdc.olga.redux.logger.Logger
import org.pyerohdc.olga.redux.logger.createLoggerMiddleware
import org.pyerohdc.olga.view.OlgaApplication
import java.io.File
import java.nio.file.Paths
import java.util.*


class Olga {

    companion object {
        @JvmStatic
        lateinit var databaseConfiguration: DatabaseConfiguration

        @JvmStatic
        private lateinit var store: Store<AppState, Action>

        @JvmStatic
        private lateinit var bus: RxBus

        @JvmStatic
        private lateinit var appProperties: AppProperties

        @JvmStatic
        val STORE
            get() = store

        @JvmStatic
        val BUS
            get() = bus

        @JvmStatic
        val APP_PROPERTIES
            get() = appProperties

        @JvmStatic
        fun main(args: Array<String>) {
            initLogger()
            initProperties()
            initStore()
            initBus()
            initValidationValueExtractor()

            Application.launch(OlgaApplication::class.java)

            databaseConfiguration.close()
        }

        @JvmStatic
        fun isDevMode() = getJarPath().toFile().isDirectory

        private fun initLogger() {
            if (isDevMode()) Log.DEBUG() else Log.INFO()
            Log.setLogger(if (isDevMode()) ConsoleLogger() else FileLogger())
            LogFactory.setFallbackLogCreator(FlywayLogger())
        }

        @JvmStatic
        fun reloadDatabaseConfiguration() {
            val databaseLocation = APP_PROPERTIES.databaseLocation
            checkNotNull(databaseLocation) { "L'emplacement de la base doit être renseigné" }

            val locationPath = Paths.get(databaseLocation)
            check(locationPath.toFile().let { it.exists() && it.canWrite() }) { "L'emplacement de la base doit être accessible en écriture : $locationPath" }

            databaseConfiguration = DatabaseConfiguration(locationPath.resolve("olga").toString())
        }

        private fun initStore() {
            store = Store(
                AppState(),
                initReducers(),
                createLoggerMiddleware(
                    predicate = { _, _ -> Log.DEBUG },
                    diffPredicate = { _, _ -> Log.TRACE },
                    diff = true,
                    logger = Logger { Log.info("Store", it.toString()) })
            )
        }

        private fun initBus() {
            bus = RxBus()
        }

        private fun initValidationValueExtractor() {
            ValueExtractor.addObservableValueExtractor(
                { control -> control is Spinner<*> },
                { control -> (control as Spinner<*>).valueProperty() })
        }

        private fun initProperties() {
            val jarFolderPath = getJarFolderPath()
            val filePropertiesPath = jarFolderPath.resolve(AppProperties.FILE_NAME)

            val jarProperties =
                Olga::class.java.classLoader.getResourceAsStream("org/pyerohdc/olga/${AppProperties.FILE_NAME}")!!
                    .use {
                        val p = Properties()
                        p.load(it)
                        p
                    }

            if (filePropertiesPath.toFile().exists()) {
                val fileProperties = filePropertiesPath
                    .toFile()
                    .inputStream()
                    .use {
                        val p = Properties()
                        p.load(it)
                        p
                    }

                fileProperties.putAll(jarProperties)

                appProperties = AppProperties(fileProperties)
                appProperties.writeProps(jarFolderPath.toFile())
            } else {
                filePropertiesPath.toFile()
                    .outputStream()
                    .use { jarProperties.store(it, null) }

                appProperties = AppProperties(jarProperties)
            }
        }

        @JvmStatic
        private fun getJarPath() = File(Olga::class.java.protectionDomain.codeSource.location.toURI()).toPath()

        @JvmStatic
        fun getJarFolderPath() =
            getJarPath().parent!!
    }

}
