package org.pyerohdc.olga.bus

import org.pyerohdc.olga.view.Components

class SwitchScene(val component: Components, override vararg val payload: Any) : RxBus.BusEvent(payload)
