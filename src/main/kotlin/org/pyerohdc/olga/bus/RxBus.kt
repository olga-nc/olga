package org.pyerohdc.olga.bus

import com.esotericsoftware.minlog.Log
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javafx.scene.Scene
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox
import javafx.stage.Stage
import org.pyerohdc.olga.entities.Batterie
import org.pyerohdc.olga.entities.Drone
import org.pyerohdc.olga.entities.Telepilote
import org.pyerohdc.olga.view.Components
import org.pyerohdc.olga.view.FXMLController
import org.pyerohdc.olga.view.MainView
import org.pyerohdc.olga.view.RestoreEntitiesView
import org.pyerohdc.olga.view.WithSubscriptions
import org.pyerohdc.olga.view.drone.EcranDrone
import org.pyerohdc.olga.view.drone.EntretienBatterie
import org.pyerohdc.olga.view.telepilote.EcranTelepilote
import org.pyerohdc.olga.view.util.Component
import org.pyerohdc.olga.view.util.FXMLLoaderUtil
import org.pyerohdc.olga.view.util.SceneProperties

class RxBus {

    abstract class BusEvent(open vararg val payload: Any)

    private val subject = PublishSubject.create<BusEvent>()

    fun asObservable(): Observable<BusEvent> = subject

    fun send(event: BusEvent) = subject.onNext(event)

    private var currentSceneController: FXMLController? = null

    fun manageBusEvent(event: BusEvent, primaryStage: Stage) {
        when (event) {
            is SwitchScene -> manageSwitchScene(event, primaryStage)
            else -> error("Not supported yet : $event")
        }
    }

    private fun manageSwitchScene(event: SwitchScene, primaryStage: Stage) {
        Log.info("EventBus", "Changement de scène : ${event.component}")

        if (currentSceneController is WithSubscriptions) {
            (currentSceneController as WithSubscriptions).disposeSubscriptions()
        }

        val component = when (event.component) {
            Components.MAIN_VIEW -> FXMLLoaderUtil.load<VBox, MainView>(event.component)
            Components.ECRAN_DRONE -> FXMLLoaderUtil.load<BorderPane, EcranDrone>(event.component)
                .also { it.controller.initData(event.payload[0] as Drone, event.payload.getOrNull(1) as? Batterie) }
            Components.ECRAN_TELEPILOTE -> FXMLLoaderUtil.load<VBox, EcranTelepilote>(event.component)
                .also { it.controller.initData(event.payload[0] as Telepilote) }
            Components.ENTRETIEN_BATTERIE -> FXMLLoaderUtil.load<BorderPane, EntretienBatterie>(event.component)
                .also { it.controller.initData(event.payload[0] as Batterie) }
            Components.RESTORE_ENTITIES_VIEW -> FXMLLoaderUtil.load<VBox, RestoreEntitiesView>(event.component)
            else -> error("Composant non supporté : ${event.component}")
        }
        currentSceneController = component.controller

        processSceneProperties(component, primaryStage)

        primaryStage.scene = Scene(component.node)
    }

    private fun processSceneProperties(component: Component<Pane, *>, primaryStage: Stage) {
        val sceneProperties =
            component.controller.javaClass.kotlin.annotations.find { it is SceneProperties } as? SceneProperties
        if (sceneProperties != null) {
            primaryStage.width = sceneProperties.prefWidth
        } else {
            primaryStage.width = Double.NaN
        }
    }

}
