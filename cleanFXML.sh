#!/usr/bin/env bash
applyRegex() {
  perl -0777 -pe "$1" "$2" >"$2".edited
  mv "$2".edited "$2"
}

for f in $(find ./src/main/resources/org/pyerohdc/olga/view -name '*.fxml' -type f); do
  applyRegex 's/(http:\/\/javafx\.com\/\w+)\/[\d.]+/$1/g' "$f"
done
